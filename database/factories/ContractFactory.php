<?php

namespace Database\Factories;

use App\Models\Contract;
use App\Models\ContractType;
use App\Models\ServiceProvider;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ContractFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contract::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $equipmentIds = ['71840dee-1fc4-47fd-a7dc-53d4e5e03100', 'f0cd4231-783e-4a53-8ac0-aefc683a8a69'];

        return [
            'equipment_id' => Arr::random($equipmentIds),
            'contract_type_id' => ContractType::all()->random()->id,
            'service_provider_id' => ServiceProvider::all()->random()->id,
            'started_at' => Carbon::now()->subDays(rand(1, 7)),
            'delay' => rand(100, 365)
        ];
    }
}
