<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        ServiceProvider::factory(5)->create();
//        ContractType::factory(5)->create();
//        Contract::factory()->create();

        $this->call([
//            CreateOperationTypesMetadataSeeder::class,
//            CreateNextStepsMetadataSeeder::class,
//            CreateOperationTitlesMetadataSeeder::class,
//            CreateEmergencyLevelSeeder::class,
//            CreateResultMetadataSeeder::class,
//            AddContractTimeRemainingToMetadataSeeder::class,
//            AddMaintenancePreventivesPlanToMetadataSeeder::class,

//            AddCodeToOperationTypesSeeder::class,
            AddCodeFieldToNextStepsMetadataSeeder::class,
        ]);


    }
}
