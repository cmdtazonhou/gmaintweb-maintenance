<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class EmergencyLevelSeeder extends Seeder
{
    use MetadataTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $metadata = Metadata::where('name', 'emergency-levels')->firstOrFail();
        $this->storeMetadataData(['name' => $faker->sentence], $metadata);
    }
}
