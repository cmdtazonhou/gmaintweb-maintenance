<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;

class CreateResultMetadataSeeder extends Seeder
{
    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Metadata::where('name', 'results')->first()) {
            $resultMetadata = Metadata::create(['name' => 'results']);
            $this->call([
                ResultSeeder::class
            ]);
        }
    }
}
