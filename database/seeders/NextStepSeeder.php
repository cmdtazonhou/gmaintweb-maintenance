<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class NextStepSeeder extends Seeder
{
    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $iterations = rand(2, 5);

        $followUpMetadata = Metadata::ofName('next-steps')->firstOrFail();

        for ($i = 0; $i < $iterations; $i++) {
            $this->storeMetadataData(['name' => $faker->word], $followUpMetadata);
        }
    }
}
