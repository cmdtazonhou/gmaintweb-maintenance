<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Database\Seeder;

class CreateOperationTypesMetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Metadata::where('name', 'operation-types')->first()) {

            $operationTypesMetadata = Metadata::create(['name' => 'operation-types']);

            $this->call([
                OperationTypeSeeder::class
            ]);

        }
    }
}
