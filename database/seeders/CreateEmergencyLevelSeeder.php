<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Database\Seeder;

class CreateEmergencyLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Metadata::where('name', 'emergency-levels')->first()) {
            $emergencyLevel = Metadata::create(['name' => 'emergency-levels']);
            $this->call([
                EmergencyLevelSeeder::class
            ]);
        }
    }
}
