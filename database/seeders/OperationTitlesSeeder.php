<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class OperationTitlesSeeder extends Seeder
{
    use MetadataTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $metadata = Metadata::where('name', 'operation-titles')->firstOrFail();
        $this->storeMetadataData(['name' => $faker->sentence], $metadata);
    }
}
