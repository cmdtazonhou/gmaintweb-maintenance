<?php

namespace Database\Seeders;

use App\Models\Material;
use App\Models\MaterialCategory;
use Illuminate\Database\Seeder;

class MaterialCategoryBuilderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $materialCategory = MaterialCategory::create([
            'name' => 'Equipment',
            'is_equipment' => true
        ]);

    }
}
