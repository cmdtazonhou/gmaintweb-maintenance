<?php

namespace Database\Seeders;

use App\Models\Breakdown;
use App\Models\Need;
use App\Models\ProformaInvoice;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PurgeMaintenanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        ProformaInvoice::truncate();
        Need::truncate();
        Breakdown::truncate();
    }
}
