<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;

class AddMaintenancePreventivesPlanToMetadataSeeder extends Seeder
{

    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $maintenancePreventivesPlan = Metadata::create(['name' => 'maintenance-preventives-plan']);
        $this->storeMetadataData([
            'ipmp_rate_min' => 0,
            'ipmp_rate_max' => 100,
            'preventive_maintenance_required' => 1
        ], $maintenancePreventivesPlan);
    }
}
