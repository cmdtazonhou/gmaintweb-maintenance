<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class OperationTypeSeeder extends Seeder
{
    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $iterations = rand(2, 5);

        $operationTypesMetadata = Metadata::ofName('operation-types')->firstOrFail();

        for ($i = 0; $i < $iterations; $i++) {
            $this->storeMetadataData(['name' => $faker->sentence], $operationTypesMetadata);
        }
    }
}
