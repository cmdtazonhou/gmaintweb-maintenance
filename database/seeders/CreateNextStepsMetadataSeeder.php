<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Database\Seeder;

class CreateNextStepsMetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (!Metadata::where('name', 'next-steps')->first()) {
            $followUpMetadata = Metadata::create(['name' => 'next-steps']);
            $this->call([
                NextStepSeeder::class
            ]);
        }
    }
}
