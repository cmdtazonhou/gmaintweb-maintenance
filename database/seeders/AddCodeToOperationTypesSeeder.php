<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class AddCodeToOperationTypesSeeder extends Seeder
{

    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $metadataCollection = Metadata::all();

        $operationTypesData = $this->getMetadataData($metadataCollection, "operation-types")->all();

        foreach ($operationTypesData as $key => $operationType) {
            $operationTypesData[$key]["code"] = $faker->numberBetween(1, 52);
        }

        $operationTypesMetadata = $metadataCollection->where("name", "operation-types")
            ->first()
            ->update(["data" => $operationTypesData]);

    }
}
