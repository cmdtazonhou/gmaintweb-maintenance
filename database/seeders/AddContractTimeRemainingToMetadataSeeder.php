<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;

class AddContractTimeRemainingToMetadataSeeder extends Seeder
{

    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $metadata = Metadata::create(['name' => 'contract-time-remaining']);
        $this->storeMetadataData([
            'value' => 1
        ], $metadata);
    }
}
