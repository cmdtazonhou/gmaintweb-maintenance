<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class AddCodeFieldToNextStepsMetadataSeeder extends Seeder
{
    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $metadataCollection = Metadata::all();

        $nextSteps = $this->getMetadataData($metadataCollection, "next-steps")->all();

        $reformKey = $faker->numberBetween(0, count($nextSteps) - 1);

        foreach ($nextSteps as $key => $nextStep) {
            $nextSteps[$key]["code"] = $key == $reformKey ? 'reformed' : 'others';
            $nextSteps[$key]["name"] = $key == $reformKey ? 'reformed' : $nextSteps[$key]["name"];
        }

        $nextStepsMetadata = $metadataCollection->where("name", "next-steps")
            ->first()
            ->update(["data" => $nextSteps]);
    }
}
