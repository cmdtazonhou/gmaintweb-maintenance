<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;

class CreateAmountMetadataSeeder extends Seeder
{
    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $resultMetadata = Metadata::create(['name' => 'amount']);
        $this->storeMetadataData(['name' => 'discount', 'value' => '1000'], $resultMetadata);
        $this->storeMetadataData(['name' => 'tva_percent', 'value' => '20'], $resultMetadata);
    }
}
