<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Database\Seeder;

class CreateOperationTitlesMetadataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (!Metadata::where('name', 'operation-titles')->first()) {
            $operationTitlesMetadata = Metadata::create(['name' => 'operation-titles']);
            $this->call([
                OperationTitlesSeeder::class
            ]);
        }
    }
}
