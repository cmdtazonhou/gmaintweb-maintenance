<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ResultSeeder extends Seeder
{
    use MetadataTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $metadata = Metadata::where('name', 'results')->firstOrFail();

        $this->storeMetadataData(['name' => 'Conforme', 'value' => 'conform'], $metadata);
        $this->storeMetadataData(['name' => 'Non Conforme', 'value' => 'improper'], $metadata);
    }
}
