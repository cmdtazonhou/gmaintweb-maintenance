<?php

namespace Database\Seeders;

use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class MetadataBuilderSeeder extends Seeder
{

    use MetadataTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        $resultMetadata = Metadata::create(['name' => 'results']);
        $this->storeMetadataData(['name' => 'Conforme', 'value' => 'conform'], $resultMetadata);
        $this->storeMetadataData(['name' => 'Non Conforme', 'value' => 'improper'], $resultMetadata);

        $resultMetadata = Metadata::create(['name' => 'amount']);
        $this->storeMetadataData(['name' => 'discount', 'value' => '1000'], $resultMetadata);
        $this->storeMetadataData(['name' => 'tva_percent', 'value' => '20'], $resultMetadata);

        $nextStep = Metadata::create(['name' => 'next-steps']);
        $this->storeMetadataData(['name' => $faker->word], $nextStep);

        $operationTypesMetadata = Metadata::create(['name' => 'operation-types']);
        $this->storeMetadataData(['name' => $faker->sentence], $operationTypesMetadata);

        $operationTitlesMetadata = Metadata::create(['name' => 'operation-titles']);
        $this->storeMetadataData(['name' => $faker->sentence], $operationTitlesMetadata);

        $emergencyLevel = Metadata::create(['name' => 'emergency-levels']);
        $this->storeMetadataData(["name" => "Travail à effectuer dans les 48h", "time_limit" => 2], $emergencyLevel);

    }
}
