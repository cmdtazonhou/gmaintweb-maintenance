<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOperationTitleScheduledInterventionDateEffectiveInterventionDatePeriodicityIdIsMaintenancePreventiveToBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('breakdowns', function (Blueprint $table) {
            $table->json('operation_title')->nullable()->after('operation_type');
            $table->timestamp('scheduled_intervention_date')->nullable()->after('scheduled_at');
            $table->timestamp('effective_intervention_date')->nullable()->after('performers');
            $table->uuid('periodicity_id')->nullable()->after('emergency_level');
            $table->boolean('is_maintenance_preventive')->nullable()->after('entity_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('breakdowns', function (Blueprint $table) {
            //
        });
    }
}
