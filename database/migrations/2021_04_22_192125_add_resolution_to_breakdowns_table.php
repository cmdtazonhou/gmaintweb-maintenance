<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResolutionToBreakdownsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('breakdowns', function (Blueprint $table) {
            $table->string('scheduled_at')->nullable()->after('emergency_level');
            $table->json('operation_type')->nullable()->after('service_provider_id');
            $table->text('diagnostic')->nullable()->after('operation_type');
            $table->text('resolution')->nullable()->after('diagnostic');
            $table->text('observations')->nullable()->after('resolution');
            $table->json('result')->nullable()->after('observations');
            $table->json('next_step')->nullable()->after('result');
            $table->string('other_cost')->nullable()->after('next_step');
            $table->json('parts')->nullable()->after('other_cost');
            $table->string('parts_cost')->nullable()->after('parts');
            $table->string('duration')->nullable()->after('parts_cost');
            $table->json('performers')->nullable()->after('duration');
            $table->string('bill_number')->nullable()->after('parts_cost');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('breakdowns', function (Blueprint $table) {
            //
        });
    }
}
