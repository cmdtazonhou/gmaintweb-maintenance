<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth.access']], function () {

    Route::resource('emergency-levels', \App\Http\Controllers\Metadata\EmergencyLevelController::class)->except(['create', 'show']);

    Route::resource('breakdowns', \App\Http\Controllers\Breakdown\BreakdownController::class)->only(['store', 'edit', 'create']);

    Route::resource('material-categories', \App\Http\Controllers\MaterialCategory\MaterialCategoryController::class)->except(['create', 'show']);

    Route::resource('materials', \App\Http\Controllers\Material\MaterialController::class)->except(['create', 'show']);

    Route::resource('material-categories.materials', \App\Http\Controllers\Material\MaterialCategoryMaterialController::class)->only(['index']);

    Route::resource('equipment.breakdowns', \App\Http\Controllers\Breakdown\EquipmentBreakdownController::class)->only(['index']);

    Route::get('entities/{entity}/breakdowns/unscheduled', [\App\Http\Controllers\Breakdown\UnscheduledController::class, 'index'])
        ->name('entities.breakdowns.unscheduled');

    Route::resource('postponed-schedule', \App\Http\Controllers\Breakdown\PostponedScheduleController::class)->only(['store']);

    Route::get('breakdowns/{breakdown}/schedule/edit', [\App\Http\Controllers\Breakdown\ScheduleController::class, 'edit'])->name('breakdown.schedule.edit');
    Route::put('breakdowns/{breakdown}/schedule', [\App\Http\Controllers\Breakdown\ScheduleController::class, 'update'])->name('breakdown.schedule.update');

    Route::get('equipment/{equipment}', [\App\Http\Controllers\Breakdown\EquipmentController::class, 'show'])->name('equipment.show');

    Route::post('breakdowns/{breakdown}/malfunction-causes', [\App\Http\Controllers\Breakdown\MalfunctionCauseController::class, 'store'])->name('breakdowns.malfunction_causes.store');

    Route::post('amount', [\App\Http\Controllers\Amount\AmountController::class, 'store'])->name('amount.store');

    Route::get('breakdowns/{breakdown}/resolution', [\App\Http\Controllers\Breakdown\ResolutionController::class, 'create'])->name('breakdowns.resolution.create');
    Route::post('breakdowns/{breakdown}/resolution', [\App\Http\Controllers\Breakdown\ResolutionController::class, 'store'])->name('breakdowns.resolution.store');

    Route::post('maintenance-in-progress/{service}', [\App\Http\Controllers\Breakdown\MaintenanceInProgressController::class, 'store'])->name('maintenance_in_progress.store');

    Route::resource('service-providers', \App\Http\Controllers\ServiceProvider\ServiceProviderController::class)->except(['create', 'show']);

    Route::resource('contract-types', \App\Http\Controllers\ContractType\ContractTypeController::class)->except(['create', 'show']);

    Route::resource('entities.contracts', \App\Http\Controllers\Contract\ContractController::class)->except(['show']);

    Route::resource('needs', \App\Http\Controllers\Need\NeedController::class)->only(['store', 'destroy', 'edit']);

    Route::resource('entities.needs', \App\Http\Controllers\Need\NeedController::class)->only(['index']);

    Route::resource('entities.validation-levels', \App\Http\Controllers\ValidationLevel\ValidationLevelController::class)->except(['create', 'show']);

    Route::resource('staff.proforma-invoices', \App\Http\Controllers\ProformaInvoice\ProformaInvoiceController::class)->only(['index']);

    Route::resource('needs.proforma-invoices', \App\Http\Controllers\ProformaInvoice\ProformaInvoiceController::class)->only(['store']);

    Route::resource('proforma-invoices', \App\Http\Controllers\ProformaInvoice\ProformaInvoiceController::class)->only(['destroy', 'edit']);

    Route::post('needs/{need}/proforma-invoices/submit', [\App\Http\Controllers\ProformaInvoice\SubmitProformaInvoiceController::class, 'store'])->name('needs.proforma_invoice.submit.store');

    Route::get('staff/{staff}/needs/awaiting-validation', [\App\Http\Controllers\Need\NeedAwaitingValidationController::class, 'index'])->name('staff.needs.awaiting_validation.index');

    Route::put('entities/{entity}/needs/{need}/proforma-invoices/validate', [\App\Http\Controllers\Need\ValidateProformaInvoiceController::class, 'update'])->name('entities.needs.proforma_invoice.validate.update');

    Route::get('maintenance-preventives/planning-settings', [\App\Http\Controllers\MaintenancePreventive\MaintenancePreventivePlanningSettingController::class, 'edit'])->name('maintenance.preventives.planning.settings.edit');
    Route::put('maintenance-preventives/planning-settings', [\App\Http\Controllers\MaintenancePreventive\MaintenancePreventivePlanningSettingController::class, 'update'])->name('maintenance.preventives.planning.settings.update');

    Route::prefix('/maintenance-preventives')->name('maintenance-preventives.')->group(function () {

        Route::get('schedule', [\App\Http\Controllers\MaintenancePreventive\ScheduleMaintenancePreventiveController::class, 'create'])->name('schedule.create');
        Route::post('schedule', [\App\Http\Controllers\MaintenancePreventive\ScheduleMaintenancePreventiveController::class, 'store'])->name('schedule.store');

        Route::get('{breakdown}/execute', [\App\Http\Controllers\MaintenancePreventive\ExecuteMaintenancePreventiveController::class, 'edit'])->name('execute.edit');
        Route::put('{breakdown}/execute', [\App\Http\Controllers\MaintenancePreventive\ExecuteMaintenancePreventiveController::class, 'update'])->name('execute.update');

    });

    Route::prefix('/metadata')->name('metadata.')->group(function () {

        Route::resource('operation-types', \App\Http\Controllers\Metadata\OperationTypeMetadataController::class)->except(['create', 'show']);

        Route::resource('operation-titles', \App\Http\Controllers\Metadata\OperationTitleMetadataController::class)->except(['create', 'show']);

        Route::get('contract-time-remaining', [\App\Http\Controllers\Metadata\ContractTimeRemainingMetadataController::class, 'edit'])->name('contract-time-remaining.edit');
        Route::put('contract-time-remaining', [\App\Http\Controllers\Metadata\ContractTimeRemainingMetadataController::class, 'update'])->name('contract-time-remaining.update');

        Route::apiResource('next-steps', \App\Http\Controllers\Metadata\NextStepController::class);

        Route::get('results', [\App\Http\Controllers\Metadata\ResultController::class, 'index'])->name('results.index');
        Route::put('results', [\App\Http\Controllers\Metadata\ResultController::class, 'update'])->name('results.update');
    });

    Route::prefix('/entities/{entity}/maintenance-preventives')->name('entities.maintenance-preventives.')->group(function () {

        Route::get('not-performed', [\App\Http\Controllers\MaintenancePreventive\MaintenancePreventiveNotPerformedController::class, 'index'])->name('not-performed');

        Route::get('calendar', [\App\Http\Controllers\MaintenancePreventive\MaintenancePreventiveCalendarController::class, 'index'])->name('calendar');

    });

    Route::prefix('/entities/{entity}/breakdowns')->name('entities.breakdowns.')->group(function () {

        Route::get('calendar', [\App\Http\Controllers\Breakdown\BreakdownCalendarController::class, 'index'])->name('calendar');

    });

    Route::prefix('/entities/{entity}/contracts')->name('entities.contracts.')->group(function () {

        Route::get('expire-soon', [\App\Http\Controllers\Contract\ContractExpireSoonController::class, 'index'])->name('expire-soon');

        Route::post('history', [\App\Http\Controllers\Contract\ContractHistoryController::class, 'store'])->name('history');

    });

    Route::prefix('contracts')->name('contracts.')->group(function () {

        Route::put('{contract}/cancel', [\App\Http\Controllers\Contract\CancelContractController::class, 'update'])->name('cancel');

        Route::post('{contract}/renewing', [\App\Http\Controllers\Contract\RenewingContractController::class, 'store'])->name('renewing');

    });

    Route::post('user-maintenance-preventive', [\App\Http\Controllers\UserMaintenancePreventive\UserMaintenancePreventiveController::class, 'store'])->name('user-maintenance-preventive.store');
    Route::get('entities/{entity}/user-maintenance-preventive/to-validate', [\App\Http\Controllers\UserMaintenancePreventive\CloseUserMaintenancePreventiveController::class, 'index'])->name('user-maintenance-preventive.to-validate.index');
    Route::get('user-maintenance-preventive/to-validate/{breakdown}/edit', [\App\Http\Controllers\UserMaintenancePreventive\CloseUserMaintenancePreventiveController::class, 'edit'])->name('user-maintenance-preventive.to-validate.edit');
    Route::put('user-maintenance-preventive/to-validate/{breakdown}', [\App\Http\Controllers\UserMaintenancePreventive\CloseUserMaintenancePreventiveController::class, 'update'])->name('user-maintenance-preventive.to-validate.update');

    Route::post('maintenance-costs', [\App\Http\Controllers\MaintenanceCosts\MaintenanceCostsController::class, 'store'])->name('maintenance-costs.store');
    Route::get('maintenance-costs/create', [\App\Http\Controllers\MaintenanceCosts\MaintenanceCostsController::class, 'create'])->name('maintenance-costs.create');

    Route::post('maintenance-parts', [\App\Http\Controllers\MaintenanceParts\MaintenancePartsController::class, 'store'])->name('maintenance-parts.store');
    Route::get('maintenance-parts/create', [\App\Http\Controllers\MaintenanceParts\MaintenancePartsController::class, 'create'])->name('maintenance-parts.create');

    Route::get('entities/{entity}/breakdowns/scheduled', [\App\Http\Controllers\Breakdown\ScheduledController::class, 'index'])
        ->name('entities.breakdowns.scheduled');

    Route::get('entities/{entity}/breakdowns/resolved', [\App\Http\Controllers\Breakdown\ResolvedController::class, 'index'])
        ->name('entities.breakdowns.resolved');

    Route::put('breakdowns/{breakdown}/validate', [\App\Http\Controllers\Breakdown\ResolvedController::class, 'update'])
        ->name('entities.breakdowns.resolved.update');

    Route::put('equipment/{equipment}/cancel-preventive-maintenance', [
        \App\Http\Controllers\MaintenancePreventive\CancelMaintenancePreventiveController::class,
        'update'
    ])->name('equipment.cancel-preventive-maintenance');

});









