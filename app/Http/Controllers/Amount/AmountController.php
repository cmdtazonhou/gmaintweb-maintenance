<?php

namespace App\Http\Controllers\Amount;

use App\Http\Controllers\Controller;
use App\Traits\AmountTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AmountController extends Controller
{

    use MetadataTrait, AmountTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'priceHT' => 'required|numeric|min:1'
        ];

        $this->validate($request, $rules);

        return response()->json(['priceTTC' => $this->amount($request->priceHT)], Response::HTTP_OK);
    }

}
