<?php

namespace App\Http\Controllers\ContractType;

use App\Http\Controllers\Controller;
use App\Models\ContractType;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContractTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(ContractType::all(), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string'
        ];

        $this->validate($request, $rules);

        $contractType = ContractType::create($request->only(['name']));

        return response()->json($contractType, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ContractType  $contractType
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(ContractType $contractType)
    {
        return response()->json(['contractType' => $contractType], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ContractType $contractType
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, ContractType $contractType)
    {
        $rules = [
            'name' => 'required|string'
        ];

        $this->validate($request, $rules);

        $contractType->update($request->only(['name']));

        return response()->json($contractType, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ContractType $contractType
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy(ContractType $contractType)
    {
        $contractType->secureDelete('contracts');

        return response()->json($contractType, Response::HTTP_OK);
    }
}
