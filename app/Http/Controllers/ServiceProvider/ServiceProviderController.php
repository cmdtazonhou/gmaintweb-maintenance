<?php

namespace App\Http\Controllers\ServiceProvider;

use App\Http\Controllers\Controller;
use App\Models\ServiceProvider;
use App\Traits\phoneNumbers;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ServiceProviderController extends Controller
{
    use phoneNumbers;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(ServiceProvider::all(), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'phone_number' => 'nullable|digits:8|unique:service_providers,phone_number,NULL,id,deleted_at,NULL|starts_with:' . implode(',', $this->phoneNumbersStartedWith()),
            'email' => 'nullable|email:rfc|unique:service_providers,email,NULL,id,deleted_at,NULL',
            'others' => 'nullable',
            'entity_id' => 'required|uuid'
        ];

        $this->validate($request, $rules);

        $serviceProvider = ServiceProvider::create($request->all());

        return response()->json($serviceProvider, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ServiceProvider $serviceProvider
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(ServiceProvider $serviceProvider)
    {
        return response()->json($serviceProvider, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param ServiceProvider $serviceProvider
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, ServiceProvider $serviceProvider)
    {
        $rules = [
            'name' => 'required|string',
            'phone_number' => "nullable|digits:8|unique:service_providers,phone_number,{$serviceProvider->id},id,deleted_at,NULL|starts_with:" . implode(',', $this->phoneNumbersStartedWith()),
            'email' => "nullable|email:rfc|unique:service_providers,email,{$serviceProvider->id},id,deleted_at,NULL",
            'others' => 'nullable'
        ];

        $this->validate($request, $rules);

        $serviceProvider->update($request->all());

        return response()->json($serviceProvider, Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ServiceProvider $serviceProvider
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy(ServiceProvider $serviceProvider)
    {
        $serviceProvider->secureDelete('contracts', 'breakdowns');
        return response()->json($serviceProvider, Response::HTTP_CREATED);
    }
}
