<?php

namespace App\Http\Controllers\Equipment;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class ReformController extends Controller
{

    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request, $equipmentId)
    {
        //
    }
}
