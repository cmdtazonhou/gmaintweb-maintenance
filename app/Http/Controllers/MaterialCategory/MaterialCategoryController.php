<?php

namespace App\Http\Controllers\MaterialCategory;

use App\Http\Controllers\Controller;
use App\Models\MaterialCategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MaterialCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['materialCategories' => MaterialCategory::all()], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:material_categories,name,NULL,id,deleted_at,NULL',
        ];

        $this->validate($request, $rules);

        $materialCategory = MaterialCategory::create([
            'name' => $request->name,
            'is_equipment' => false
        ]);

        return response()->json($materialCategory, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\MaterialCategory $materialCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(MaterialCategory $materialCategory)
    {
        return response()->json(['materialCategory' => $materialCategory], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\MaterialCategory $materialCategory
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, MaterialCategory $materialCategory)
    {
        $rules = [
            'name' => "required|unique:material_categories,name,{$materialCategory->id},id,deleted_at,NULL",
        ];

        $this->validate($request, $rules);

        $materialCategory->update(['name' => $request->name]);

        return response()->json($materialCategory, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\MaterialCategory $materialCategory
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy(MaterialCategory $materialCategory)
    {
        if ($materialCategory->is_equipment) {
            return response()->json(['error' => 'Can not remove this materialCategory', 'code' => Response::HTTP_CONFLICT], Response::HTTP_CONFLICT);
        }

        $materialCategory->secureDelete('materials');

        return response()->json($materialCategory, Response::HTTP_OK);
    }
}
