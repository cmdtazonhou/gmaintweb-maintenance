<?php

namespace App\Http\Controllers\ValidationLevel;

use App\Http\Controllers\Controller;
use App\Models\ValidationLevel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidationLevelController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($entityId)
    {
        return response()->json(ValidationLevel::where('entity_id', $entityId)->get(), Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $entityId)
    {
        $rules = [
            'staff_id' => 'required|uuid',
        ];

        $this->validate($request, $rules);

        $request->merge([
            'entity_id' => $entityId,
            'level' => ValidationLevel::where('entity_id', $entityId)->get()->count() + 1
        ]);

        $validationLevel = ValidationLevel::create($request->all());

        return response()->json($validationLevel, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ValidationLevel $validationLevel
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($entityId, ValidationLevel $validationLevel)
    {
        return response()->json(['validationLevel' => $validationLevel], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $entityId
     * @param \App\Models\ValidationLevel $validationLevel
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $entityId, ValidationLevel $validationLevel)
    {
        $rules = [
            'staff_id' => 'required|uuid',
        ];

        $this->validate($request, $rules);

        $request->merge([
            'entity_id' => $entityId,
            'level' => $validationLevel->level
        ]);

        $validationLevel->update($request->all());

        return response()->json($validationLevel, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $entityId
     * @param \App\Models\ValidationLevel $validationLevel
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy($entityId, ValidationLevel $validationLevel)
    {
        if (ValidationLevel::where('entity_id', $entityId)->where('level', (intval($validationLevel->level) + 1))->get()->count() != 0) {
            return response()->json(['error' => "Can not delete this validation level"], Response::HTTP_NOT_FOUND);
        }

        $validationLevel->secureDelete();;

        return response()->json($validationLevel, Response::HTTP_OK);
    }
}
