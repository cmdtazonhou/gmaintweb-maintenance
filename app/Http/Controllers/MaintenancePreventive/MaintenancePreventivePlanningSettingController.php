<?php

namespace App\Http\Controllers\MaintenancePreventive;

use App\Http\Controllers\Controller;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MaintenancePreventivePlanningSettingController extends Controller
{

    use MetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return JsonResponse
     */
    public function edit()
    {
        $metadataList = Metadata::all();

        return response()->json(
            $this->getMetadataData($metadataList, 'maintenance-preventives-plan')->first(),
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $rules = [
            'ipmp_rate_min' => 'required|integer|numeric|min:0',
            'ipmp_rate_max' => 'required|integer|numeric|min:0|gt:ipmp_rate_min',
            'preventive_maintenance_required' => 'required|boolean'
        ];

        $this->validate($request, $rules);

        $request->merge(['preventive_maintenance_required' => intval($request->preventive_maintenance_required)]);

        $metadataList = Metadata::all();

        $maintenancePreventivesSettings = $this->getMetadataData($metadataList, 'maintenance-preventives-plan')->first();

        $metadata = $metadataList->where('name', 'maintenance-preventives-plan')->first();
        $metadata = $this->updateMetadataData(
            $request->only(['ipmp_rate_min', 'ipmp_rate_max', 'preventive_maintenance_required']),
            $metadata,
            $maintenancePreventivesSettings['id']
        );

        if ($metadata === false){
            return response()->json([
                'error' => "Can't perform this action, verify your inputs", 'code' => JsonResponse::HTTP_BAD_REQUEST],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return response()->json($request->only(['ipmp_rate_min', 'ipmp_rate_max', 'preventive_maintenance_required']), JsonResponse::HTTP_OK);

    }

}
