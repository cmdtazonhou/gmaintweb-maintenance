<?php

namespace App\Http\Controllers\MaintenancePreventive;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenancePreventive\ExecuteMaintenancePreventiveRequest;
use App\Models\Breakdown;
use App\Models\ServiceProvider;
use App\Services\MaintenancePreventiveService;
use App\Traits\ApiResponser;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ExecuteMaintenancePreventiveController extends Controller
{

    use MetadataTrait;
    use ApiResponser;
    use ContractTrait;
    use BreakdownTrait;

    /**
     * @var MaintenancePreventiveService
     */
    private $maintenancePreventiveService;

    public function __construct(MaintenancePreventiveService $maintenancePreventiveService)
    {
        $this->middleware('auth.access');
        $this->maintenancePreventiveService = $maintenancePreventiveService;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Breakdown $breakdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Breakdown $breakdown)
    {
        $data = $this->getBreakdown($breakdown);
        $data['serviceProviders'] = ServiceProvider::ofEntity($breakdown['entity_id'])->get();
        return $this->successResponse($data, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Breakdown $breakdown
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(ExecuteMaintenancePreventiveRequest $request, Breakdown $breakdown)
    {
        $upcomingBreakdown = $this->maintenancePreventiveService->updateMaintenancePreventive($request->all(), $breakdown);
        return $this->successResponse($upcomingBreakdown, Response::HTTP_CREATED);
    }

}
