<?php

namespace App\Http\Controllers\MaintenancePreventive;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenancePreventive\cancelMaintenancePreventiveRequest;
use App\Services\MaintenancePreventiveService;
use App\Traits\ApiResponser;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CancelMaintenancePreventiveController extends Controller
{

    use ApiResponser;

    /**
     * Update the specified resource in storage.
     *
     * @param cancelMaintenancePreventiveRequest $request
     * @param  $equipmentId
     * @return JsonResponse
     */
    public function update(cancelMaintenancePreventiveRequest $request, $equipmentId)
    {
        $maintenancePreventives = (new MaintenancePreventiveService())->cancelMaintenancePreventive(
            $equipmentId,
            $request->validated()
        );

        return $this->successResponse($maintenancePreventives, JsonResponse::HTTP_OK);
    }

}
