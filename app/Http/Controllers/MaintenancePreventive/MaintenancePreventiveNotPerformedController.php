<?php

namespace App\Http\Controllers\MaintenancePreventive;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MaintenancePreventiveNotPerformedController extends Controller
{

    use MetadataTrait;
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entity_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($entity_id)
    {
        $breakdowns = Breakdown::with('needs')
            ->preventiveMaintenance()
            ->localised($entity_id)
            ->deadlineExceeded()
            ->where('status', Breakdown::SCHEDULED)
            ->get();

        return $this->successResponse($breakdowns, Response::HTTP_OK);
    }

}
