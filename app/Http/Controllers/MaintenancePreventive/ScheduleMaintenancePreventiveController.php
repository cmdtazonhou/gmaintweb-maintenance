<?php

namespace App\Http\Controllers\MaintenancePreventive;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenancePreventive\ScheduleMaintenancePreventiveRequest;
use App\Services\MaintenancePreventiveService;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ScheduleMaintenancePreventiveController extends Controller
{

    use MetadataTrait;
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $metadataList = Metadata::all();

        $data['maintenancePreventivePlan'] = $this->getMetadataData($metadataList, 'maintenance-preventives-plan')->first();
        $data['operationTypes'] = $this->getMetadataData($metadataList, "operation-types");
        $data['operationTitles'] = $this->getMetadataData($metadataList, "operation-titles");

        return response()->json(
            $data,
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ScheduleMaintenancePreventiveRequest $request
     * @param MaintenancePreventiveService $maintenancePreventiveService
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(
        ScheduleMaintenancePreventiveRequest $request,
        MaintenancePreventiveService $maintenancePreventiveService
    )
    {
        $request->ensureItCanBeScheduled();

        $breakdown = $maintenancePreventiveService->scheduleMaintenancePreventive($request->validated());

        return $this->successResponse($breakdown, Response::HTTP_CREATED);
    }

}
