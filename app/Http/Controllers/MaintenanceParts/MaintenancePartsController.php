<?php

namespace App\Http\Controllers\MaintenanceParts;

use App\Http\Controllers\Controller;
use App\Http\Requests\Maintenance\FilterMaintenanceRequest;
use App\Models\Breakdown;
use App\Services\FilterMaintenanceService;
use App\Traits\ApiResponser;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use App\Traits\FilterTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MaintenancePartsController extends Controller
{

    use MetadataTrait;
    use ApiResponser;
    use BreakdownTrait;
    use ContractTrait;
    use FilterTrait;

    /**
     * Show the form for creating a new resource.
     *
     * @return JsonResponse
     */
    public function create()
    {
        return $this->successResponse([
            'maintenance_types' => $this->getMaintenanceTypes(),
            'years' => $this->getYears(),
            'months' => $this->getMonths()
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FilterMaintenanceRequest $request
     * @return JsonResponse
     */
    public function store(FilterMaintenanceRequest $request)
    {
        return $this->successResponse([
            'breakdowns' => (new FilterMaintenanceService())->filterMaintenance($request)
        ], JsonResponse::HTTP_OK);
    }

}
