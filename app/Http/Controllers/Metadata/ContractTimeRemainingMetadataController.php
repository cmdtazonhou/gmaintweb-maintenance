<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContractTimeRemainingMetadataController extends Controller
{

    use MetadataTrait;
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit()
    {
        $data = $this->getMetadataData(Metadata::all(), 'contract-time-remaining')->all()[0];
        return $this->successResponse($data, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request)
    {
        $rules = [
            'value' => ['required', 'integer', 'numeric', 'min:1']
        ];

        $this->validate($request, $rules);

        $metadata = Metadata::where('name', 'contract-time-remaining')->firstOrFail();

        $this->updateMetadataData(
            ['value' => intval($request->value)],
            $metadata,
            $metadata->data[0]['id']
        );

        return $this->successResponse($request->only(['value']), Response::HTTP_OK);
    }


}
