<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use App\Http\Requests\Metadata\UpdateResultRequest;
use App\Services\ResultService;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ResultController extends Controller
{

    use MetadataTrait;
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $data = $this->getMetadataData(Metadata::all(), 'results');
        return $this->successResponse($data, JsonResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateResultRequest $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateResultRequest $request)
    {
        (new ResultService())->update($request->validated());
        return $this->successResponse($request->validated(), JsonResponse::HTTP_OK);
    }

}
