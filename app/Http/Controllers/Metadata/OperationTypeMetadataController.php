<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Rules\EmergencyLevelWithTimeLimitZero;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Rules\Unique;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class OperationTypeMetadataController extends Controller
{
    use MetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(
            $this->getMetadataData(Metadata::all(), 'operation-types'),
            Response::HTTP_OK
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', new Unique('operation-types', 'name')],
            'code' => ['required', new Unique('operation-types', 'code')],
        ];

        $this->validate($request, $rules);

        $metadata = Metadata::where('name', 'operation-types')->firstOrFail();

        $this->storeMetadataData($request->only(['name', 'code']), $metadata);

        return response()->json($request->only(['name', 'code']), Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return response()->json(
            $this->findData('operation-types', $id),
            Response::HTTP_CREATED
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => ['required', new Unique('operation-types', 'name', false, true, $id)],
            'code' => ['required', new Unique('operation-types', 'code', false, true, $id)]
        ];

        $this->validate($request, $rules);

        $metadata = Metadata::where('name', 'operation-types')->firstOrFail();

        $this->updateMetadataData($request->only(['name', 'code']), $metadata, $id);

        return response()->json($request->only(['name', 'code']), Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $metadata = Metadata::where('name', 'operation-types')->firstOrFail();

        $this->destroyMetadataData($metadata, $id);

        return response()->json(['success' => 'Deleted successfully'], Response::HTTP_OK);
    }
}
