<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Rules\EmergencyLevelWithTimeLimitZero;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Rules\Unique;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EmergencyLevelController extends Controller
{
    use MetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json($this->getMetadataData(Metadata::all(), 'emergency-levels'), Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', new Unique('emergency-levels', 'name')],
            'time_limit' => ['required', 'integer', 'numeric', new EmergencyLevelWithTimeLimitZero()]
        ];

        $this->validate($request, $rules);

        $metadata = Metadata::where('name', 'emergency-levels')->firstOrFail();

        $this->storeMetadataData($request->only(['name', 'time_limit']), $metadata);

        return response()->json($request->only(['name', 'time_limit']), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return response()->json(['emergencyLevel' => $this->findData('emergency-levels', $id)], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => ['required', new Unique('emergency-levels', 'name', false, true, $id)],
            'time_limit' => ['required', 'integer', 'numeric', new EmergencyLevelWithTimeLimitZero($id)]
        ];

        $this->validate($request, $rules);

        $metadata = Metadata::where('name', 'emergency-levels')->firstOrFail();

        $this->updateMetadataData($request->only(['name', 'time_limit']), $metadata, $id);

        return response()->json($request->only(['name', 'time_limit']), Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (Breakdown::where('emergency_level->dataId', $id)->get()->count() > 0) {
            return response()->json(
                ['error' => 'Can not delete this resource now. It\'s used by another resource',
                    'code' => Response::HTTP_CONFLICT],
                Response::HTTP_CONFLICT
            );
        }

        $metadata = Metadata::where('name', 'emergency-levels')->firstOrFail();

        $this->destroyMetadataData($metadata, $id);

        return response()->json(['success' => 'Deleted successfully'], Response::HTTP_OK);
    }
}
