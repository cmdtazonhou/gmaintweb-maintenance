<?php

namespace App\Http\Controllers\Metadata;

use App\Http\Controllers\Controller;
use App\Http\Requests\Metadata\StoreNextStepRequest;
use App\Http\Requests\Metadata\UpdateNextStepRequest;
use App\Services\NextStepService;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NextStepController extends Controller
{

    use MetadataTrait;
    use ApiResponser;

    public $service;

    /**
     * NextStepController constructor.
     * @param NextStepService $service
     */
    public function __construct(NextStepService $service)
    {
        $this->middleware('auth.access');
        $this->service = $service;
    }


    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $data = $this->getMetadataData(Metadata::all(), 'next-steps');
        return $this->successResponse($data, JsonResponse::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreNextStepRequest $request
     * @return JsonResponse
     */
    public function store(StoreNextStepRequest $request)
    {
        $this->service->create($request->validated());
        return $this->successResponse($request->validated(), JsonResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        return $this->successResponse($this->service->find($id), JsonResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNextStepRequest $request
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function update(UpdateNextStepRequest $request, $id)
    {
        $this->service->update($request->validated(), $id);
        return $this->successResponse($request->validated(), JsonResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        return $this->successResponse($this->service->delete($id), JsonResponse::HTTP_OK);
    }
}
