<?php

namespace App\Http\Controllers\Material;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\MaterialCategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MaterialCategoryMaterialController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param MaterialCategory $materialCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(MaterialCategory $materialCategory)
    {
        return response()->json(['materials' => $materialCategory->materials], Response::HTTP_OK);
    }
}
