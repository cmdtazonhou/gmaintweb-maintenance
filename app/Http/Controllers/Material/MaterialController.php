<?php

namespace App\Http\Controllers\Material;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\MaterialCategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MaterialController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['materials' => Material::with('materialCategory')->get()], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:materials,name,NULL,id,deleted_at,NULL',
            'material_category_id' => 'required|exists:material_categories,id',
            'entity_id' => 'required|uuid'
        ];

        $this->validate($request, $rules);

        $material = Material::create($request->only([
            'name',
            'material_category_id',
            'entity_id'
        ]));

        return response()->json($material, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Material $material
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Material $material)
    {
        $material->load('materialCategory');
        return response()->json(['material' => $material, 'materialCategories' => MaterialCategory::all()], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Material $material
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Material $material)
    {
        $rules = [
            'name' => "required|unique:materials,name,{$material->id},id,deleted_at,NULL",
            'material_category_id' => 'required|exists:material_categories,id'
        ];

        $this->validate($request, $rules);

        $material->update($request->only([
            'name',
            'material_category_id'
        ]));

        return response()->json($material, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Material $material
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy(Material $material)
    {
        $material->secureDelete('breakdowns');

        return response()->json($material, Response::HTTP_OK);
    }
}
