<?php

namespace App\Http\Controllers\Contract;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Traits\ApiResponser;
use App\Traits\ContractTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Symfony\Component\HttpFoundation\Response;

class ContractExpireSoonController extends Controller
{

    use MetadataTrait;
    use ApiResponser;
    use ContractTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entity_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($entity_id)
    {
        $contracts = Contract::with(['contractType', 'serviceProvider'])
            ->owner($entity_id)
            ->get()
            ->filter(function ($contract, $key) {
                $contract->expiry_date = $this->contractExpiryDate($contract);
                return $this->expireSoon($contract) && !$contract->archived_at;
            })
            ->values();

        return $this->successResponse($contracts, Response::HTTP_OK);
    }

}
