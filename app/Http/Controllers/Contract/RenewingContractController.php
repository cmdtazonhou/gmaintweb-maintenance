<?php

namespace App\Http\Controllers\Contract;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Rules\AfterFrenchDate;
use App\Traits\ApiResponser;
use App\Traits\ContractTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RenewingContractController extends Controller
{
    use MetadataTrait;
    use ApiResponser;
    use ContractTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Contract $contract
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, Contract $contract)
    {

        $rules = [
            'started_at' => [
                'required',
                new AfterFrenchDate($this->contractExpiryDate($contract)->format('d/m/Y'))
            ],
            'delay' => 'required|integer|numeric|min:1',
        ];

        $this->validate($request, $rules);

        $contract->update(['archived_at' => Carbon::now()]);

        $newContract = Contract::create([
            'equipment_id' => $contract->equipment_id,
            'contract_type_id' => $contract->contract_type_id,
            'service_provider_id' => $contract->service_provider_id,
            'entity_id' => $contract->entity_id,
            'started_at' => Carbon::createFromFormat('d/m/Y', $request->started_at),
            'delay' => $request->delay
        ]);

        return $this->successResponse($newContract, Response::HTTP_OK);
    }
}
