<?php

namespace App\Http\Controllers\Contract;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contract\StoreContractRequest;
use App\Models\Breakdown;
use App\Models\Contract;
use App\Models\ContractType;
use App\Models\ServiceProvider;
use App\Rules\BelongsTo;
use App\Rules\UnderContract;
use App\Services\ContractService;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContractController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($entityId)
    {
        return response()->json(
            Contract::with(['contractType', 'serviceProvider'])->where('entity_id', $entityId)->get()
            , Response::HTTP_OK
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function create($entityId)
    {
        return response()->json(
            [
                'contractTypes' => ContractType::all(),
                'serviceProviders' => ServiceProvider::where('entity_id', $entityId)->get()
            ]
            , Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreContractRequest $request
     * @param $entityId
     * @param ContractService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreContractRequest $request, $entityId, ContractService $service)
    {
        $contract = $service->storeContract($request->validated());
        return response()->json($contract, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Contract $contract
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($entityId, Contract $contract)
    {
        return response()->json(
            [
                'contractTypes' => ContractType::all(),
                'serviceProviders' => ServiceProvider::where('entity_id', $entityId)->get(),
                'contract' => $contract->load(['contractType', 'serviceProvider']),
                'breakdownsCount' => Breakdown::where('equipment_id', $contract->equipment_id)->get()->count()
            ]
            , Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $entityId
     * @param \App\Models\Contract $contract
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $entityId, Contract $contract)
    {
        $rules = [
            'equipment_id' => ['required', 'uuid', new UnderContract($contract->id)],
            'contract_type_id' => 'required|uuid|exists:contract_types,id',
            'service_provider_id' => [
                'required',
                'uuid',
                'exists:service_providers,id',
                new BelongsTo('service_providers', 'entity_id', $entityId)
            ],
            'started_at' => 'required|date_format:Y-m-d',
            'delay' => 'required|integer|numeric|min:1',
        ];

        $this->validate($request, $rules);

        $request->merge(['entity_id' => $entityId]);

        $contract->update($request->only([
            'equipment_id',
            'contract_type_id',
            'service_provider_id',
            'started_at',
            'delay',
            'entity_id'
        ]));

        return response()->json($contract, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Contract $contract
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy($entityId, Contract $contract)
    {
        $contract->secureDelete();
        return response()->json($contract, Response::HTTP_OK);
    }
}
