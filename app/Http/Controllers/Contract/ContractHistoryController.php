<?php

namespace App\Http\Controllers\Contract;

use App\Http\Controllers\Controller;
use App\Models\Contract;
use App\Models\ServiceProvider;
use App\Rules\AfterFrenchDate;
use App\Rules\BeforeFrenchDate;
use App\Traits\ApiResponser;
use App\Traits\ContractTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class ContractHistoryController extends Controller
{

    use MetadataTrait;
    use ApiResponser;
    use ContractTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $entity_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $entity_id)
    {
        $rules = [
            'start' => [
                Rule::requiredIf($request->filled('end')),
                new BeforeFrenchDate($request->end)
            ],
            'end' => [
                Rule::requiredIf($request->filled('start')),
                new AfterFrenchDate($request->start)
            ],
            'event_in_period' => [
                Rule::requiredIf($request->filled('start') && $request->filled('end')),
                Rule::in(['signed', 'expired', 'signed_and_expired'])
            ],
            'status' => [
                'nullable',
                Rule::in(['archived', 'expired', 'in-progress'])
            ],
            'service_provider_id' => [
                'nullable',
                'exists:service_providers,id'
            ]
        ];

        $this->validate($request, $rules);

        try {
            $request->merge([
                'start' => Carbon::createFromFormat('d/m/Y', $request->start),
                'end' => Carbon::createFromFormat('d/m/Y', $request->end)
            ]);
        }catch (\Exception $exception){}

        $contracts = Contract::with(['contractType', 'serviceProvider'])
            ->owner($entity_id)
            ->get()
            ->filter(function ($contract, $key) use ($request) {
                $contract->expiry_date = $this->contractExpiryDate($contract);
                $contract->status = $this->getStatus($contract);
                return $this->filterContract($request, $contract);
            })
            ->values();

        $serviceProviders = ServiceProvider::ofEntity($entity_id)->get();

        return $this->successResponse(compact('contracts', 'serviceProviders'), Response::HTTP_OK);
    }

}
