<?php

namespace App\Http\Controllers\Need;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Models\Need;
use App\Models\ValidationLevel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NeedController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($entityId)
    {
        return response()->json(
            Need::with('breakdown.materialCategory', 'breakdown.material', 'breakdown.serviceProvider')
                ->get()
                ->filter(function ($need, $key) use ($entityId) {
                    return $need->breakdown->entity_id == $entityId && $need->breakdown->status != 'done';
                })
                ->values(),
            Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'breakdown_id' => 'required|uuid|exists:breakdowns,id',
            'parts' => 'required|array',
            'parts.*.amount' => 'required|integer|numeric|min:1',
            'parts.*.name' => 'required|string',
        ];

        $this->validate($request, $rules);

        $need = Need::create([
            'breakdown_id' => $request->breakdown_id,
            'parts' => $request->parts,
            'status' => 'opened',
            'validation_level_id' => ValidationLevel::where('level', 1)->first()->id,
        ]);

        return response()->json($need, Response::HTTP_CREATED);
    }

    /**
     * @param Need $need
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Need $need)
    {
        return response()->json($need->load([
            'breakdown.materialCategory',
            'breakdown.material',
            'breakdown.serviceProvider',
            'proformaInvoices',
            'proformaInvoiceChosen'
        ]), Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Need $need
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy(Need $need)
    {
        $need->secureDelete('proformaInvoices');
        return response()->json($need, Response::HTTP_OK);
    }
}
