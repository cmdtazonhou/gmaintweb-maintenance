<?php

namespace App\Http\Controllers\Need;

use App\Http\Controllers\Controller;
use App\Models\Need;
use App\Models\ProformaInvoice;
use App\Models\ValidationLevel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateProformaInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $entityId
     * @param \App\Models\Need $need
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $entityId, Need $need)
    {
        $need->load('proformaInvoices', 'breakdown');

        $rules = [
            'proforma_invoice_id' => [
                'required',
                'exists:proforma_invoices,id',
                function ($attribute, $value, $fail) use ($need) {
                    if (!in_array($value, $need->proformaInvoices->pluck('id')->toArray())) {
                        $fail('The ' . $attribute . ' does not belong to the specific need.');
                    }
                },
            ]
        ];

        $this->validate($request, $rules);

        $data = [];
        $data['proforma_invoice_id'] = $request->proforma_invoice_id;

        // If it's the last validation step
        if ($need->validationLevel->level == ValidationLevel::where('entity_id', $entityId)->count()) {
            $data['status'] = 'closed';

            // add parts and parts_cost to the breakdown associated with the need

            $previewPartsCost = intval($need->breakdown->parts_cost);

            $proformaInvoiceChosen = ProformaInvoice::find($request->proforma_invoice_id);

            $parts = $need->breakdown->parts ?: [];

            foreach ($proformaInvoiceChosen->parts as $part) {
                array_push($parts, $part);
            }

            $need->breakdown->update([
                'parts' => $parts,
                'parts_cost' => $previewPartsCost + intval($proformaInvoiceChosen->total_ttc)
            ]);
        }

        // If it's not the last validation step
        if ($need->validationLevel->level != ValidationLevel::where('entity_id', $entityId)->count()) {
            $data['validation_level_id'] = ValidationLevel::where('level', (intval($need->validationLevel->level) + 1))->first()->id;
        }

        $need->update($data);

        return response()->json($need, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Need $need
     * @return \Illuminate\Http\Response
     */
    public function destroy(Need $need)
    {
        //
    }
}
