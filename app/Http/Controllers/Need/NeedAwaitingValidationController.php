<?php

namespace App\Http\Controllers\Need;

use App\Http\Controllers\Controller;
use App\Models\Need;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class NeedAwaitingValidationController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $staffId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($staffId)
    {
        return response()->json(
            Need::with(['breakdown.materialCategory', 'breakdown.material', 'breakdown.serviceProvider', 'validationLevel'])
                ->get()
                ->filter(function ($need, $key) use ($staffId) {
                    return $need->validationLevel->staff_id == $staffId && $need->status == 'invoices-added';
                })
                ->values(),
            Response::HTTP_OK
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Need  $need
     * @return \Illuminate\Http\Response
     */
    public function show(Need $need)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Need  $need
     * @return \Illuminate\Http\Response
     */
    public function edit(Need $need)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Need  $need
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Need $need)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Need  $need
     * @return \Illuminate\Http\Response
     */
    public function destroy(Need $need)
    {
        //
    }
}
