<?php

namespace App\Http\Controllers\UserMaintenancePreventive;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserMaintenancePreventive\StoreUserMaintenancePreventiveRequest;
use App\Services\UserMaintenancePreventiveService;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;

class UserMaintenancePreventiveController extends Controller
{

    use MetadataTrait;
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserMaintenancePreventiveRequest $request
     * @return JsonResponse
     */
    public function store(StoreUserMaintenancePreventiveRequest $request)
    {
        $breakdown = (new UserMaintenancePreventiveService())->storeUserMaintenancePreventive($request);
        return $this->successResponse($breakdown, JsonResponse::HTTP_CREATED);
    }

}
