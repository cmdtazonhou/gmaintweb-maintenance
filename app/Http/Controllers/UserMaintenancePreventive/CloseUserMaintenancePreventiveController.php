<?php

namespace App\Http\Controllers\UserMaintenancePreventive;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Traits\ApiResponser;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CloseUserMaintenancePreventiveController extends Controller
{

    use MetadataTrait;
    use ApiResponser;
    use BreakdownTrait;
    use ContractTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entity_id
     * @return JsonResponse
     */
    public function index($entity_id)
    {
        $breakdowns = Breakdown::localised($entity_id)
            ->userPreventiveMaintenance()
            ->where('status', Breakdown::DONE)
            ->get();

        return $this->successResponse($breakdowns, JsonResponse::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Breakdown $breakdown
     * @return JsonResponse
     */
    public function edit(Breakdown $breakdown)
    {
        return $this->successResponse($this->getBreakdown($breakdown), JsonResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Breakdown $breakdown
     * @return JsonResponse
     */
    public function update(Request $request, Breakdown $breakdown)
    {
        $breakdown->update([
            'status' => Breakdown::ARCHIVED,
            'resolved_at' => Carbon::now()
        ]);

        return $this->successResponse($breakdown, JsonResponse::HTTP_OK);
    }

}
