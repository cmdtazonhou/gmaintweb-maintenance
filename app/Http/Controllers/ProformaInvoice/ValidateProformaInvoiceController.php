<?php

namespace App\Http\Controllers\ProformaInvoice;

use App\Http\Controllers\Controller;
use App\Models\ProformaInvoice;
use App\Models\ValidationLevel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateProformaInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ProformaInvoice $proformaInvoice
     * @return \Illuminate\Http\Response
     */
    public function show(ProformaInvoice $proformaInvoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ProformaInvoice $proformaInvoice
     * @return \Illuminate\Http\Response
     */
    public function edit(ProformaInvoice $proformaInvoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProformaInvoice $proformaInvoice
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, ProformaInvoice $proformaInvoice)
    {
        $rules = [
            'validate' => 'required|boolean',
        ];

        $this->validate($request, $rules);

        if ($request->validate && $proformaInvoice->validationLevel->level == ValidationLevel::all()->count()) {

            $data['status'] = 'validated';

        }

        if ($request->validate && $proformaInvoice->validationLevel->level != ValidationLevel::all()->count()){

            $data['validation_level_id'] = ValidationLevel::where('level', (intval($proformaInvoice->level)+1))->first()->id;

        }

        if (!$request->validate){

            $data['status'] = 'invalidated';

        }

        $proformaInvoice->update($data);

        return response()->json($proformaInvoice, Response::HTTP_OK);
    }

}
