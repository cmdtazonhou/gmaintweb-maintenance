<?php

namespace App\Http\Controllers\ProformaInvoice;

use App\Http\Controllers\Controller;
use App\Models\Need;
use App\Models\ProformaInvoice;
use App\Models\ValidationLevel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SubmitProformaInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Need $need
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Need $need)
    {
        $need->update([
            'status' => 'invoices-added',
        ]);
        return response()->json($need, Response::HTTP_OK);
    }

}
