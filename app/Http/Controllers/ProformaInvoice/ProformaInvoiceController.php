<?php

namespace App\Http\Controllers\ProformaInvoice;

use App\Http\Controllers\Controller;
use App\Models\Need;
use App\Models\ProformaInvoice;
use App\Models\ValidationLevel;
use App\Rules\MatchNeeds;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProformaInvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $staffId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($staffId)
    {
        return response()->json(
            ProformaInvoice::with('need.breakdown', 'validationLevel')
                ->get()
                ->filter(function ($proformaInvoice, $key) use ($staffId) {
                    return $proformaInvoice->validationLevel->staff_id == $staffId && $proformaInvoice->status == 'to-validate';
                })
                ->values(),
            Response::HTTP_OK
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Need $need
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, Need $need)
    {
        $rules = [
            'reference' => 'required|string',
            'provider' => 'required|string',
            'phone_number' => 'required|string',
            'parts' => ['required', 'array', new MatchNeeds($need)],
            'parts.*.amount' => 'required|integer|numeric|min:1',
            'parts.*.name' => 'required|string',
            'parts.*.unit_price' => 'required|integer|numeric|min:1',
            'discount' => 'nullable|integer|numeric|min:1',
            'tva_percent' => 'nullable|integer|numeric|min:1',
        ];

        $this->validate($request, $rules);

        $totalHt = 0;
        foreach ($request->parts as $part) {
            $totalHt += intval($part['amount']) * intval($part['unit_price']);
        }

        $totalAfterDiscount = $totalHt - intval($request->discount);

        $request->merge([
            'discount' => $request->discount,
            'total_ht' => $totalHt,
            'tva_percent' => $request->tva_percent,
            'total_ttc' => $totalAfterDiscount + ($totalAfterDiscount * (intval($request->tva_percent) / 100)),
            'need_id' => $need->id
        ]);

        $proformaInvoice = ProformaInvoice::create($request->all());

        return response()->json($proformaInvoice, Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ProformaInvoice $proformaInvoice
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(ProformaInvoice $proformaInvoice)
    {
        return response()->json($proformaInvoice->load('need.breakdown', 'validationLevel'), Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ProformaInvoice $proformaInvoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProformaInvoice $proformaInvoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ProformaInvoice $proformaInvoice
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\SecureDeleteException
     */
    public function destroy(ProformaInvoice $proformaInvoice)
    {
        $proformaInvoice->secureDelete();
        return response()->json($proformaInvoice, Response::HTTP_OK);
    }
}
