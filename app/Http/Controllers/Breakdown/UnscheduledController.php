<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\ServiceProvider;
use App\Services\CorrectiveMaintenanceService;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UnscheduledController extends Controller
{
    use ApiResponser;
    use MetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entityId
     * @param CorrectiveMaintenanceService $service
     * @return JsonResponse
     */
    public function index($entityId, CorrectiveMaintenanceService $service)
    {
        $data = [
            'breakdowns' => $service->getUnscheduledMaintenanceCorrectives($entityId)
        ];

        return $this->successResponse($data, JsonResponse::HTTP_OK);
    }

}
