<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Http\Requests\Maintenance\ResolveMaintenanceRequest;
use App\Models\Breakdown;
use App\Traits\AmountTrait;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use App\Traits\ResolutionTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ResolutionController extends Controller
{
    use ResolutionTrait, MetadataTrait, ContractTrait, BreakdownTrait, AmountTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    public function create(Breakdown $breakdown)
    {
        $data = $this->getBreakdown($breakdown);

        $metadata = Metadata::all();

        $data['emergency_levels'] = $this->getMetadataData($metadata, 'emergency-levels');
        $data['operation_types'] = $this->getMetadataData($metadata, 'operation-types');
        $data['results'] = $this->getMetadataData($metadata, 'results');
        $data['next_steps'] = $this->getMetadataData($metadata, 'next-steps');

        $amountMetadata = $this->getMetadataData(Metadata::all(), 'amount');

        $data['discount'] = intval($this->getAmountParam($amountMetadata, 'discount')['value']);
        $data['tva_percent'] = intval($this->getAmountParam($amountMetadata, 'tva_percent')['value']);

        return response()->json($data, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ResolveMaintenanceRequest $request
     * @param Breakdown $breakdown
     * @return JsonResponse
     */
    public function store(ResolveMaintenanceRequest $request, Breakdown $breakdown)
    {
        $breakdown->update($request->extractData());
        $breakdown->load('materialCategory');
        return response()->json($breakdown, Response::HTTP_OK);
    }

}
