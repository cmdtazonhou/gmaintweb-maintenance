<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenanceCorrective\StoreMaintenanceCorrectiveRequest;
use App\Models\Breakdown;
use App\Models\MaterialCategory;
use App\Models\ServiceProvider;
use App\Services\CorrectiveMaintenanceService;
use App\Traits\ApiResponser;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use App\Traits\SelfMetadataTrait;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BreakdownController extends Controller
{

    use ContractTrait;
    use MetadataTrait;
    use BreakdownTrait;
    use SelfMetadataTrait;
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }


    public function create()
    {
        return $this->successResponse([
            'materialCategories' => MaterialCategory::all(),
            'emergencyLevels' => $this->getMetadataData(Metadata::all(), 'emergency-levels')->values(),
            'serviceProviders' => ServiceProvider::all()
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMaintenanceCorrectiveRequest $request
     * @param CorrectiveMaintenanceService $service
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreMaintenanceCorrectiveRequest $request, CorrectiveMaintenanceService $service)
    {
        $request->ensureEquipmentIsFunctional();

        $breakdown = $service->storeMaintenanceCorrective($request->validated());

        if (!$breakdown->status) {
            $breakdown = $service->scheduleMaintenanceCorrective($request->validated(), $breakdown);
        }

        return response()->json($breakdown, Response::HTTP_CREATED);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Breakdown $breakdown
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Breakdown $breakdown)
    {
        return response()->json($this->getBreakdown($breakdown), Response::HTTP_OK);
    }

}
