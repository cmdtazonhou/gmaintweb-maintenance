<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Rules\MaterialExists;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class EquipmentBreakdownController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $equipmentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($equipmentId)
    {
        return response()->json(['breakdowns' => Breakdown::where('equipment_id', $equipmentId)->get()], Response::HTTP_OK);
    }

}
