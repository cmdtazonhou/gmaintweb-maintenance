<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Traits\SelfMetadataTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MaintenanceInProgressController extends Controller
{

    use MetadataTrait, SelfMetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $service
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, $service)
    {
        $request->merge(['service' => $service]);

        $rules = [
            'rooms' => 'required|array',
            'rooms.*' => 'uuid',
            'service' => 'required|in:external,internal'
        ];

        $this->validate($request, $rules);

        $breakdowns = Breakdown::with('materialCategory', 'material')
            ->nonPreventiveMaintenance()
            ->where('status', 'in-progress')
            ->where('emergency_level->dataId', '!=', optional($this->getEmergencyLevelWithTimeLimitZero())['id'])
            ->whereIn('room_id', $request->rooms)
            ->where('service', $service)
            ->get();

        return response()->json(['breakdowns' => $breakdowns], Response::HTTP_OK);
    }
}
