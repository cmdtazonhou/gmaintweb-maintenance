<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Traits\SelfMetadataTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PostponedScheduleController extends Controller
{

    use MetadataTrait, SelfMetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'rooms' => 'required|array',
            'rooms.*' => 'uuid'
        ];

        $this->validate($request, $rules);

        $emergencyLevelWithTimeLimitZero = $this->getEmergencyLevelWithTimeLimitZero();

        $breakdowns = Breakdown::with('materialCategory', 'material')
            ->nonPreventiveMaintenance()
            ->where('status', 'in-progress')
            ->whereIn('room_id', $request->rooms)
            ->where('emergency_level->dataId', $emergencyLevelWithTimeLimitZero['id'])
            ->get();

        return response()->json(['breakdowns' => $breakdowns], Response::HTTP_OK);
    }

}
