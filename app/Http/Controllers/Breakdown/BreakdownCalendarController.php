<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Traits\ApiResponser;
use Symfony\Component\HttpFoundation\Response;

class BreakdownCalendarController extends Controller
{

    use ApiResponser;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entity_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($entity_id)
    {
        $breakdowns = Breakdown::with(['material', 'materialCategory'])
            ->nonPreventiveMaintenance()
            ->localised($entity_id)
            ->whereIn('status', [Breakdown::SCHEDULED])
            ->get();

        return $this->successResponse($breakdowns, Response::HTTP_OK);
    }

}
