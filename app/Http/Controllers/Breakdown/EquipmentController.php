<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Models\Contract;
use App\Models\Material;
use App\Rules\MaterialExists;
use App\Traits\ContractTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class EquipmentController extends Controller
{

    use ContractTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display the specified resource.
     *
     * @param $equipmentId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($equipmentId)
    {
        return response()->json([
            'availableContract' => optional($this->getEquipmentAvailableContract($equipmentId))->load('serviceProvider', 'contractType'),
            'breakdownsCount' => Breakdown::where('equipment_id', $equipmentId)->get()->count(),
            'equipment' => Material::with('materialCategory')->find($equipmentId)
        ], Response::HTTP_OK);
    }

}
