<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class InterventionRequestsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $rules = [
            'rooms' => 'required|array',
            'rooms.*' => 'uuid'
        ];

        $this->validate($request, $rules);

        $breakdowns = Breakdown::with('materialCategory', 'material')
            ->nonPreventiveMaintenance()
            ->where('status', 'to-do')
            ->whereIn('room_id', $request->rooms)
            ->get();

        return response()->json(['breakdowns' => $breakdowns], Response::HTTP_OK);
    }

}
