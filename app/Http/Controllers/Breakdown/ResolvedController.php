<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Services\CorrectiveMaintenanceService;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ResolvedController extends Controller
{

    use ApiResponser;
    use MetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Display a listing of the resource.
     *
     * @param $entityId
     * @param CorrectiveMaintenanceService $service
     * @return JsonResponse
     */
    public function index($entityId, CorrectiveMaintenanceService $service)
    {
        $data = [
            'breakdowns' => $service->getResolvedMaintenanceCorrectives($entityId)
        ];

        return $this->successResponse($data, JsonResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Breakdown  $breakdown
     * @return JsonResponse
     */
    public function update(Request $request, Breakdown $breakdown)
    {
        $breakdown->update(['status' => Breakdown::ARCHIVED]);
        return $this->successResponse($breakdown, JsonResponse::HTTP_OK);
    }

}
