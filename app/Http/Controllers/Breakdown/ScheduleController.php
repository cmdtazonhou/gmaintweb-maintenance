<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Http\Requests\MaintenanceCorrective\ScheduleMaintenanceActivityRequest;
use App\Models\Breakdown;
use App\Models\ServiceProvider;
use App\Services\CorrectiveMaintenanceService;
use App\Traits\ApiResponser;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use App\Traits\SelfMetadataTrait;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ScheduleController extends Controller
{
    use ContractTrait;
    use MetadataTrait;
    use SelfMetadataTrait;
    use ApiResponser;
    use BreakdownTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * @param Breakdown $breakdown
     * @return JsonResponse
     */
    public function edit(Breakdown $breakdown)
    {
        $data = $this->getBreakdown($breakdown);

        $data['emergencyLevels'] = $this->getMetadataData(Metadata::all(), 'emergency-levels')
            ->where('time_limit', '!=', 0);

        $data['serviceProviders'] = ServiceProvider::all();

        return $this->successResponse($data, JsonResponse::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ScheduleMaintenanceActivityRequest $request
     * @param \App\Models\Breakdown $breakdown
     * @param CorrectiveMaintenanceService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ScheduleMaintenanceActivityRequest $request, Breakdown $breakdown, CorrectiveMaintenanceService $service)
    {
        $breakdown = $service->scheduleMaintenanceCorrective($request->validated(), $breakdown);
        return response()->json($breakdown, Response::HTTP_OK);
    }

}
