<?php

namespace App\Http\Controllers\Breakdown;

use App\Http\Controllers\Controller;
use App\Models\Breakdown;
use App\Traits\SelfMetadataTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MalfunctionCauseController extends Controller
{

    use MetadataTrait, SelfMetadataTrait;

    public function __construct()
    {
        $this->middleware('auth.access');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Breakdown $breakdown
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, Breakdown $breakdown)
    {
        // Check if the breakdown is already scheduled
        if ($breakdown->status == 'in-progress' && ($breakdown->emergency_level)['dataId'] != $this->getEmergencyLevelWithTimeLimitZero()['id']) {

            $rules = [
                'causes' => 'required|string',
                'needs' => 'required|string',
                'recommendations' => 'required|string',
                'next_step' => 'required|string',
            ];

            $this->validate($request, $rules);

            $malfunctionCauses = $breakdown->malfunction_causes;
            $malfunctionCauses[] = $request->only(['causes', 'needs', 'recommendations', 'next_step']);

            $breakdown->update(['malfunction_causes' => $malfunctionCauses]);

            return response()->json($malfunctionCauses, Response::HTTP_OK);
        }

        return response()->json([
            'error' => 'Can not perform this operation. Probable causes are : the maintenance is not yet scheduled, the breakdown is already resolved',
            'code' => Response::HTTP_UNAUTHORIZED
        ], Response::HTTP_UNAUTHORIZED);

    }

}
