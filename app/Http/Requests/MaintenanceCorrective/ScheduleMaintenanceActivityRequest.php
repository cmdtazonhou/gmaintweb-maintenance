<?php

namespace App\Http\Requests\MaintenanceCorrective;

use App\Services\CorrectiveMaintenanceService;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Foundation\Http\FormRequest;

class ScheduleMaintenanceActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'emergency_level' => ['required', new MetadataExists('emergency-levels'), 'uuid'],
        ];

        return array_merge(
            $rules,
            (new CorrectiveMaintenanceService())->getScheduleMaintenanceCorrectiveRules($this->all())
        );
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $breakdown = $this->route('breakdown');

        $this->merge(['equipment_id' =>  $breakdown['equipment_id']]);

        $this->merge((new CorrectiveMaintenanceService())->calculateDataForMaintenanceCorrectiveScheduling($this->all()));
    }

}
