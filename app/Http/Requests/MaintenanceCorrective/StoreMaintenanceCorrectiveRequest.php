<?php

namespace App\Http\Requests\MaintenanceCorrective;

use App\Models\Breakdown;
use App\Rules\MaterialExists;
use App\Services\CorrectiveMaintenanceService;
use App\Traits\ContractTrait;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StoreMaintenanceCorrectiveRequest extends FormRequest
{
    use ContractTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'material_category_id' => 'required|exists:material_categories,id',
            'equipment_id' => ['required', 'uuid', new MaterialExists($this->material_category_id)],
            'description' => 'required|string',
            'palliative_actions' => 'nullable|string',
            'room_id' => 'required|uuid',
            'created_by' => 'required|uuid',
            'entity_id' => 'required|uuid',
            'emergency_level' => [
                'nullable',
                new MetadataExists('emergency-levels'),
                'uuid'
            ]
        ];

        return array_merge(
            $rules,
            (new CorrectiveMaintenanceService())->getScheduleMaintenanceCorrectiveRules($this->all())
        );
    }

    /**
     * @throws ValidationException
     */
    public function ensureEquipmentIsFunctional()
    {
        $countCorrectiveMaintenanceInProgress = Breakdown::where('equipment_id', $this->equipment_id)
            ->localised($this->entity_id)
            ->nonPreventiveMaintenance()
            ->notArchived()
            ->count();

        if ($countCorrectiveMaintenanceInProgress > 0) {
            throw ValidationException::withMessages([
                'equipment_id' => __('Involved in unresolved corrective maintenance'),
            ]);
        }

        return;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $data = [];

        if ($this->filled('created_by')) {
            $data = ['scheduled_by' => $this->created_by];
        }

        $this->merge(array_merge(
            $data,
            (new CorrectiveMaintenanceService())->calculateDataForMaintenanceCorrectiveScheduling($this->all())
        ));
    }
}
