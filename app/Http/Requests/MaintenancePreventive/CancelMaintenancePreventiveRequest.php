<?php

namespace App\Http\Requests\MaintenancePreventive;

use Illuminate\Foundation\Http\FormRequest;

class cancelMaintenancePreventiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'archived_by' => ['required', 'uuid']
        ];
    }
}
