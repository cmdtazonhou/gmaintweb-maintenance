<?php

namespace App\Http\Requests\MaintenancePreventive;

use App\Models\Breakdown;
use App\Rules\FrenchDateTimeFormat;
use App\Traits\ApiResponser;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ScheduleMaintenancePreventiveRequest extends FormRequest
{
    use ApiResponser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_id' => 'required|uuid',
            'equipment_id' => 'required|uuid',
            'periodicity_id' => 'required|uuid',
            'operation_type' => ['required', new MetadataExists('operation-types'), 'uuid'],
            'operation_title' => ['required', new MetadataExists('operation-titles'), 'uuid'],
            'scheduled_intervention_date' => ['required', new FrenchDateTimeFormat(false, true)],
            'created_by' => 'required|uuid',
            'entity_id' => 'required|uuid',
            'duration' => 'nullable|integer|numeric|min:1',
        ];
    }

    /**
     * @throws ValidationException
     */
    public function ensureItCanBeScheduled()
    {
        $unresolvedMaintenancePreventives = Breakdown::preventiveMaintenance()
            ->notArchived()
            ->where('equipment_id', $this->equipment_id)
            ->where("operation_type->dataId", $this->operation_type)
            ->count();

        if ($unresolvedMaintenancePreventives > 0) {
            throw ValidationException::withMessages([
                'equipment_id' => __('Involved in unresolved preventive maintenance'),
            ]);
        }

        return;
    }
}
