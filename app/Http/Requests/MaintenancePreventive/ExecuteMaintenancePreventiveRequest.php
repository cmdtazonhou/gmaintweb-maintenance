<?php

namespace App\Http\Requests\MaintenancePreventive;

use App\Models\Breakdown;
use App\Rules\AfterFrenchDateTimeFormat;
use App\Rules\FrenchDateTimeFormat;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ExecuteMaintenancePreventiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $breakdown = $this->route('breakdown');

        return [
            'other_cost' => ['required', 'integer', 'numeric', 'min:0'],
            'is_done' => ['required', 'boolean'],
            'actions_performed' => [Rule::requiredIf($this->is_done), 'array'],
            'actions_performed.*' => ['uuid'],
            'effective_intervention_date' => [
                Rule::requiredIf($this->is_done),
                new FrenchDateTimeFormat(true),
                new AfterFrenchDateTimeFormat($breakdown->created_at->format('d-m-Y H:i'))
            ],
            'upcoming_schedule_intervention_date' => [
                Rule::requiredIf($this->is_done),
                new FrenchDateTimeFormat(false, true)
            ],
            'service' => [Rule::requiredIf($this->is_done), 'in:internal,external'],
            'resolved_by' => [Rule::requiredIf($this->is_done), 'uuid'],
            'performers' => [Rule::requiredIf($this->service == 'internal'), 'array'],
            'performers.*' => ['uuid'],
            'service_provider_id' => [Rule::requiredIf($this->service == 'external'), 'exists:service_providers,id', 'uuid'],
            'service_provider_performer' => [Rule::requiredIf($this->service == 'external'), 'string']
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'is_done' => $this->boolean('is_done')
        ]);
    }
}
