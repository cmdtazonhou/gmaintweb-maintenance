<?php

namespace App\Http\Requests\Contract;

use App\Rules\AfterFrenchDate;
use App\Rules\BelongsTo;
use App\Rules\UnderContract;
use App\Traits\ContractTrait;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class StoreContractRequest extends FormRequest
{

    use ContractTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'equipment_id' => ['required', 'uuid', new UnderContract],
            'contract_type_id' => 'required|uuid|exists:contract_types,id',
            'service_provider_id' => [
                'required',
                'uuid',
                'exists:service_providers,id',
                new BelongsTo('service_providers', 'entity_id', $this->route('entity'))
            ],
            'started_at' => ['required', new AfterFrenchDate(Carbon::now()->format('d/m/Y'))],
            'delay' => 'required|integer|numeric|min:1',
            'entity_id' => ['required']
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'entity_id' => $this->route('entity')
        ]);
    }

    /**
     * @throws ValidationException
     */
    public function ensureEquipmentIsNotUnderContract()
    {
        if ($this->getEquipmentAvailableContract($this->equipment_id)) {
            throw ValidationException::withMessages([
                'equipment_id' => 'Is under contract'
            ]);
        }
    }
}
