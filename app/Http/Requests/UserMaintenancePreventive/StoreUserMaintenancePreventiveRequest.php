<?php

namespace App\Http\Requests\UserMaintenancePreventive;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserMaintenancePreventiveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity_id' => 'required|uuid',
            'equipment_id' => 'required|uuid',
            'room_id' => 'required|uuid',
            'observations' => 'nullable|string',
            'actions_performed' => 'required|array',
            'actions_performed.*' => 'uuid',
            'created_by' => 'required|uuid'
        ];
    }
}
