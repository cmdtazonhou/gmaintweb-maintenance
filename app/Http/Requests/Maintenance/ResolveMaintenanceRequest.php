<?php

namespace App\Http\Requests\Maintenance;

use App\Models\Breakdown;
use App\Traits\ResolutionTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ResolveMaintenanceRequest extends FormRequest
{
    use ResolutionTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $breakdown = $this->route('breakdown');

        return [
            'operation_type' => [Rule::requiredIf($this->resolved), new MetadataExists('operation-types'), 'uuid'],
            'diagnostic' => [Rule::requiredIf($this->resolved), 'string'],
            'resolution' => [Rule::requiredIf($this->resolved), 'string'],
            'observations' => [Rule::requiredIf($this->resolved), 'string'],
            'result' => [Rule::requiredIf($this->resolved), new MetadataExists('results'), 'uuid'],
            'next_step' => [Rule::requiredIf($this->resolved), new MetadataExists('next-steps'), 'uuid'],
            'other_cost' => ['nullable', 'integer', 'numeric', 'min:0'],
            'parts_cost' => ['nullable', 'integer', 'numeric', 'min:0'],
            'parts' => ['nullable', 'array'],
            'parts.*.number' => ['required', 'integer', 'numeric', 'min:1'],
            'parts.*.name' => ['required', 'string'],
            'parts.*.unit_price' => ['required', 'numeric', 'min:1'],
            'duration' => [Rule::requiredIf($this->resolved), 'integer', 'numeric', 'min:1'],
            'performers' => [Rule::requiredIf($this->resolved && $breakdown->service == 'internal'), 'array'],
            'performers.*' => ['uuid'],
            'bill_number' => [Rule::requiredIf($this->resolved && $breakdown->service == 'external'), 'string'],
            'resolved' => ['required', 'boolean'],
        ];
    }

    public function extractData()
    {
        $breakdown = $this->route('breakdown');
        $data = [];

        $metadataData = [
            'operation_type' => 'operation-types',
            'result' => 'results',
            'next_step' => 'next-steps'
        ];

        foreach ($metadataData as $field => $metadataName) {
            $data[$field] = ['metadataName' => $metadataName, 'dataId' => $this->{$field}];
        }

        $simpleFields = [
            'diagnostic',
            'resolution',
            'observations',
            'other_cost',
            'parts_cost',
            'duration',
            'resolved_at'
        ];

        if ($breakdown->service == 'internal') {
            $simpleFields[] = 'performers';
        }

        if ($breakdown->service == 'external') {
            $simpleFields[] = 'bill_number';
        }

        foreach ($simpleFields as $field) {
            $data[$field] = $this->{$field};
        }

        if ($this->resolved) {
            $data['status'] = Breakdown::DONE;
            $data['resolved_at'] = Carbon::now();
        }

        return $data;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'resolved' => $this->boolean('resolved')
        ]);
    }

}
