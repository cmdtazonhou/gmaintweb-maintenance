<?php

namespace App\Http\Requests\Maintenance;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FilterMaintenanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'room_ids' => 'nullable|array',
            'room_ids.*' => 'uuid',
            'year' => 'nullable|digits:4',
            'month' => [
                'nullable',
                Rule::in(['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']),
            ],
            'maintenance_type' => [
                'nullable',
                Rule::in(['preventive', 'corrective', 'others']),
            ],
        ];
    }
}
