<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;

class AuthenticateAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $validSecrets = explode(',', Config::get('services.auth.secrets'));

        if(in_array($request->header('X-Authorization'), $validSecrets)){
            return $next($request);
        }

        abort(Response::HTTP_UNAUTHORIZED, 'Unauthorized');

    }
}
