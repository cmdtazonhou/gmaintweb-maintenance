<?php

namespace App\Models;

use Carbon\Carbon;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Cmdtaz\Metadata\Traits\UuidAsId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Breakdown extends Model
{
    use HasFactory, UuidAsId, SoftDeletes, MetadataTrait;

    const UNSCHEDULED = 'unscheduled';
    const RESCHEDULED = 'rescheduled';
    const SCHEDULED = 'scheduled';
    const DONE = 'done';
    const ARCHIVED = 'archived';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'equipment_id',
        'description',
        'emergency_level',
        'resolved_at',
        'material_category_id',
        'room_id',
        'palliative_actions',
        'status',
        'service',
        'service_provider_id',
        'malfunction_causes',
        'resolution',
        'scheduled_at',
        'operation_type',
        'diagnostic',
        'resolution',
        'observations',
        'result',
        'next_step',
        'other_cost',
        'parts',
        'parts_cost',
        'duration',
        'performers',
        'bill_number',
        'created_by',
        'entity_id',
        'operation_title',
        'scheduled_intervention_date',
        'effective_intervention_date',
        'periodicity_id',
        'actions_performed',
        'service_provider_performer',
        'resolved_by',
        'maintenance_type',
        'archived_by',
        'archived_at',
        'scheduled_by',
        'canceled_reason',
        'canceled_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'resolved_at' => 'datetime',
        'emergency_level' => 'json',
        'malfunction_causes' => 'json',
        'resolution' => 'json',
        'scheduled_at' => 'datetime',
        'operation_type' => 'json',
        'result' => 'json',
        'next_step' => 'json',
        'parts' => 'json',
        'performers' => 'json',
        'operation_title' => 'json',
        'scheduled_intervention_date' => 'datetime',
        'effective_intervention_date' => 'datetime',
        'actions_performed' => 'json',
        'archived_at' => 'datetime',
        'canceled_at' => 'datetime',
    ];

    /**
     * Get the activePilot flag for the staff.
     *
     * @return bool
     */
    public function getResponseTimeAttribute()
    {
        if (!isset($this->attributes['scheduled_at'])) {
            return null;
        }

        return is_null($this->attributes['scheduled_at'])
            ? null
            : (Carbon::parse($this->attributes['scheduled_at']))->diffInMinutes(Carbon::parse($this->attributes['created_at']));
    }

    /**
     * Get the activePilot flag for the staff.
     *
     * @return bool
     */
    public function getTheoreticalDurationAttribute()
    {

        if (!isset($this->attributes['scheduled_at'])) {
            return null;
        }

        return is_null($this->attributes['scheduled_at'])
            ? null
            : Carbon::now()->diffInHours(Carbon::parse($this->attributes['scheduled_at']));
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'response_time',
        'theoretical_duration'
    ];

    /**
     * Get the materialCategory that owns the breakdown.
     */
    public function materialCategory()
    {
        return $this->belongsTo(MaterialCategory::class);
    }

    /**
     * Get the material that owns the breakdown.
     */
    public function material()
    {
        return $this->belongsTo(Material::class, 'equipment_id');
    }

    /**
     * Get the serviceProvider that owns the breakdown.
     */
    public function serviceProvider()
    {
        return $this->belongsTo(ServiceProvider::class);
    }

    /**
     * Get the needs associated to the breakdown.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function needs()
    {
        return $this->hasMany(Need::class);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $equipmentId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnresolved($query, $equipmentId)
    {
        return $query->where('status', '!=', self::ARCHIVED)
            ->where('equipment_id', $equipmentId);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePreventiveMaintenance($query)
    {
        return $query->where('maintenance_type', 'preventive');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNonPreventiveMaintenance($query)
    {
        return $query->where('maintenance_type', 'non-preventive');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUserPreventiveMaintenance($query)
    {
        return $query->where('maintenance_type', 'user-preventive');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $entity_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLocalised($query, $entity_id)
    {
        return $query->where('entity_id', $entity_id);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnresolvedList($query)
    {
        return $query->whereNull('resolved_at');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDeadlineExceeded($query)
    {
        return $query->where('scheduled_intervention_date', '<', Carbon::now()->toDateString());
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeScheduled($query)
    {
        return $query->whereNotNull('scheduled_at');
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotArchived($query)
    {
        return $query->whereNull('archived_at')
            ->where('status', '!=', self::ARCHIVED);
    }


}
