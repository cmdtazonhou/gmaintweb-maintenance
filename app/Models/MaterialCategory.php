<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Cmdtaz\Metadata\Traits\UuidAsId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialCategory extends Model
{
    use HasFactory, UuidAsId, SoftDeletes, SecureDelete;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'is_equipment',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_equipment' => 'boolean',
    ];

    /**
     * Get the materials for the materialCategory.
     */
    public function materials()
    {
        return $this->hasMany(Material::class);
    }

}
