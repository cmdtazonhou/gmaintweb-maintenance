<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Cmdtaz\Metadata\Traits\UuidAsId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Need extends Model
{
    use HasFactory, UuidAsId, SoftDeletes, SecureDelete;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'breakdown_id',
        'status',
        'parts',
        'validation_level_id',
        'proforma_invoice_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'parts' => 'array'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function breakdown()
    {
        return $this->belongsTo(Breakdown::class);
    }

    /**
     * Get the proforma invoices associated to the need
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function proformaInvoices()
    {
        return $this->hasMany(ProformaInvoice::class);
    }

    /**
     * Get the post that owns the comment.
     */
    public function validationLevel()
    {
        return $this->belongsTo(ValidationLevel::class);
    }

    /**
     * Get the proforma invoice chosen for the need.
     */
    public function proformaInvoiceChosen()
    {
        return $this->belongsTo(ProformaInvoice::class, 'proforma_invoice_id');
    }

}
