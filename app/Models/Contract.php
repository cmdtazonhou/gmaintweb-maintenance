<?php

namespace App\Models;

use App\Traits\SecureDelete;
use Cmdtaz\Metadata\Traits\UuidAsId;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use HasFactory, UuidAsId, SoftDeletes, SecureDelete;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'equipment_id',
        'contract_type_id',
        'service_provider_id',
        'started_at',
        'delay',
        'entity_id',
        'archived_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'started_at' => 'datetime',
        'archived_at' => 'datetime'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * Get the contractType that owns the contract.
     */
    public function contractType()
    {
        return $this->belongsTo(ContractType::class);
    }

    /**
     * Get the serviceProvider that owns the contract.
     */
    public function serviceProvider()
    {
        return $this->belongsTo(ServiceProvider::class);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param $entity_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOwner($query, $entity_id)
    {
        return $query->where('entity_id', $entity_id);
    }

    /**
     * Scope a query to only include users of a given type.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNotArchived($query)
    {
        return $query->whereNull('archived_at');
    }


}
