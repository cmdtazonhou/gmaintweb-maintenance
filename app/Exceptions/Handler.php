<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{

    use ApiResponser;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });

        $this->renderable(function (ValidationException $e, $request) {
            return response()->json(['error' => $e->validator->errors()->getMessages(), 'code' => Response::HTTP_UNPROCESSABLE_ENTITY], Response::HTTP_UNPROCESSABLE_ENTITY);
        });

        $this->renderable(function (ModelNotFoundException $e, $request) {
            $modelName = Str::lower(class_basename($e->getModel()));
            return response()->json(['error' => "Does not exist any $modelName with the specified identifiable", 'code' => Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
        });

        $this->renderable(function (AuthenticationException $e, $request) {
            return response()->json(['error' => "Unauthenticated", 'code' => Response::HTTP_UNAUTHORIZED], Response::HTTP_UNAUTHORIZED);
        });

        $this->renderable(function (AuthorizationException $e, $request) {
            return response()->json(['error' => $e->getMessage(), 'code' => Response::HTTP_FORBIDDEN], Response::HTTP_FORBIDDEN);
        });

        $this->renderable(function (NotFoundHttpException $e, $request) {
            return response()->json(['error' => 'The specified URL cannot be found', 'code' => Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
        });

        $this->renderable(function (MethodNotAllowedHttpException $e, $request) {
            return response()->json(['error' => "The specified method for the request is invalid", 'code' => Response::HTTP_METHOD_NOT_ALLOWED], Response::HTTP_METHOD_NOT_ALLOWED);
        });

        $this->renderable(function (HttpException $e, $request) {
            return response()->json(['error' => $e->getMessage(), 'code' => $e->getStatusCode()], $e->getStatusCode());
        });

        $this->renderable(function (QueryException $e, $request) {
            if ($e->errorInfo[1] == 1451) {
                return response()->json(
                    ['error' => "Cannot remove this resource permanently. It is related with any other resource", 'code' => Response::HTTP_CONFLICT]
                    , Response::HTTP_CONFLICT
                );
            }

            return response()->json(
                ['error' => $e->errorInfo[2], 'code' => Response::HTTP_INTERNAL_SERVER_ERROR]
                , Response::HTTP_INTERNAL_SERVER_ERROR
            );
        });

        $this->renderable(function (SecureDeleteException $e, $request) {

            $modelName = Str::lower(class_basename($e->getModel()));

            return response()->json(
                ['error' => "Cannot remove this instance of {$modelName} permanently. It is related with any other resource",
                    'code' => Response::HTTP_NOT_FOUND]
                , Response::HTTP_INTERNAL_SERVER_ERROR
            );
        });

        $this->renderable(function (\Exception $e, $request) {
            return response()->json(['error' => $e->getMessage(), 'code' => $e->getCode()], $e->getCode());
        });

        return response()->json(
            ['error' => "Unexpected exception. Try later", 'code' => Response::HTTP_INTERNAL_SERVER_ERROR]
            , Response::HTTP_INTERNAL_SERVER_ERROR
        );

    }
}
