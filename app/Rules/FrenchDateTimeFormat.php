<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class FrenchDateTimeFormat implements Rule
{
    /**
     * @var bool
     */
    private $beforeToday;

    private $message;
    /**
     * @var bool
     */
    private $afterToday;

    /**
     * Create a new rule instance.
     *
     * @param bool $beforeToday
     * @param bool $afterToday
     */
    public function __construct($beforeToday = false, $afterToday = false)
    {
        $this->beforeToday = $beforeToday;
        $this->afterToday = $afterToday;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {

            $carbonDate = Carbon::createFromFormat('d-m-Y H:i', $value);

            $this->message = $this->beforeToday ? "The date value is not before today" : "The date value is not after today";

            return (!$this->beforeToday ?: ($this->beforeToday && Carbon::now()->gte($carbonDate)))
                && (!$this->afterToday ?: ($this->afterToday && Carbon::now()->lte($carbonDate)));

        } catch (\Exception $exception) {

            $this->message = "It is not a valid date";

            return false;

        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
