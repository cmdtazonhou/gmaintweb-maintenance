<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AfterFrenchDateTimeFormat implements Rule
{

    private $message;

    public $secondDate;

    /**
     * Create a new rule instance.
     *
     * @param $secondDate
     */
    public function __construct($secondDate)
    {
        $this->secondDate = $secondDate;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {

            $this->message = "It is not a valid date";
            $carbonDate = Carbon::createFromFormat('d-m-Y H:i', $value);

            $this->message = "The second date not a valid date";
            $carbonSecondDate = Carbon::createFromFormat('d-m-Y H:i', $this->secondDate);

            $this->message = "The date value is not after ".$carbonSecondDate->format('d-m-Y H:i');

            return $carbonDate->gte($carbonSecondDate);

        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
