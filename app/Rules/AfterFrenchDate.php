<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AfterFrenchDate implements Rule
{

    private $message;
    private $secondDate;

    /**
     * Create a new rule instance.
     *
     * @param $secondDate
     */
    public function __construct($secondDate)
    {
        $this->secondDate = $secondDate;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $this->message = "The $attribute value is not a valid date";
            $date = Carbon::createFromFormat('d/m/Y', $value);

            $this->message = "The second date value is not a valid date";
            $secondDate = Carbon::createFromFormat('d/m/Y', $this->secondDate);

            $this->message = "$attribute date value is not after the second one";
            return $date->gt($secondDate);

        } catch (\Exception $exception) {

            return false;

        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
