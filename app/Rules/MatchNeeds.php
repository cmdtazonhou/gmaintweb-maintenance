<?php

namespace App\Rules;

use App\Models\Need;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class MatchNeeds implements Rule
{
    /**
     * @var Need
     */
    private $need;
    /**
     * @var string
     */
    private $message;

    /**
     * Create a new rule instance.
     *
     * @param Need $need
     */
    public function __construct(Need $need)
    {
        $this->need = $need;
    }

    protected function search($needPart, $parts)
    {
        return collect($parts)->search(function ($item, $key) use ($needPart) {
            try {
                return ($item['name'] == $needPart['name']) && ($item['amount'] == $needPart['amount']);
            } catch (\Exception $exception) {
                return false;
            }
        });
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!is_array($value)) {
            $this->message = 'The parts must be an array.';
            return false;
        }

        $this->message = "$attribute does not matches need parts format";
        foreach ($this->need->parts as $part) {
            if ($this->search($part, $value) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
