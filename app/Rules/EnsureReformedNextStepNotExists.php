<?php

namespace App\Rules;

use App\Services\NextStepService;
use Illuminate\Contracts\Validation\Rule;

class EnsureReformedNextStepNotExists implements Rule
{

    private $message;
    /**
     * @var bool
     */
    public $id;

    /**
     * Create a new rule instance.
     *
     * @param bool $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->message = 'Already exist a reform next step';

        if ($value == 'reformed') {

            $reformNextStep = (new NextStepService())->getReformedNextStep();

            if (!$reformNextStep) {
                return true;
            }

            return $this->id == $reformNextStep['id'];
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
