<?php

namespace App\Rules;

use App\Models\Material;
use App\Models\MaterialCategory;
use Illuminate\Contracts\Validation\Rule;

class MaterialExists implements Rule
{
    private $materialCategoryId;
    private $message;

    /**
     * Create a new rule instance.
     *
     * @param $materialCategoryId
     */
    public function __construct($materialCategoryId)
    {
        //
        $this->materialCategoryId = $materialCategoryId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $materialCategory = MaterialCategory::find($this->materialCategoryId);

        $this->message = "Does not exists any materialCategory with this specify identificator";
        if (is_null($materialCategory)) {
            return false;
        }

        if (!$materialCategory->is_equipment) {

            $material = Material::find($value);

            $this->message = "Does not exists any material with this specify identificator";
            if (is_null($material)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
