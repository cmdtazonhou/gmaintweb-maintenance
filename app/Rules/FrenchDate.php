<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class FrenchDate implements Rule
{

    private $message;

    /**
     * Create a new rule instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $this->message = "The $attribute value is not a valid date";

            $date = Carbon::createFromFormat('d/m/Y', $value);

            return true;

        } catch (\Exception $exception) {

            return false;

        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
