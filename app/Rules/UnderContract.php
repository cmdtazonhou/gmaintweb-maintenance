<?php

namespace App\Rules;

use App\Models\Contract;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class UnderContract implements Rule
{
    /**
     * @var null
     */
    private $contractId;

    /**
     * Create a new rule instance.
     * @param null $contractId
     */
    public function __construct($contractId = null)
    {
        $this->contractId = $contractId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Contract::where('equipment_id', $value)
                ->get()
                ->search(function ($contract, $key) {
                    return Carbon::parse($contract->started_at)->addMonths(intval($contract->delay))->gte(Carbon::now());
                }) === false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The selected equipment is already under contract';
    }
}
