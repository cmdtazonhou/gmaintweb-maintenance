<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class BelongsTo implements Rule
{
    private $tableName;
    private $resourceOwnerId;
    private $resourceOwnerFieldName;

    /**
     * Create a new rule instance.
     *
     * @param $tableName
     * @param $resourceOwnerFieldName
     * @param $resourceOwnerId
     */
    public function __construct($tableName, $resourceOwnerFieldName, $resourceOwnerId)
    {
        $this->tableName = $tableName;
        $this->resourceOwnerId = $resourceOwnerId;
        $this->resourceOwnerFieldName = $resourceOwnerFieldName;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return DB::table($this->tableName)
            ->where('id', $value)
            ->where($this->resourceOwnerFieldName, $this->resourceOwnerId)
            ->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The selected service provider is not correct';
    }
}
