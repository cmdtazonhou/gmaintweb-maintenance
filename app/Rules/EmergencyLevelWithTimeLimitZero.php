<?php

namespace App\Rules;

use App\Traits\SelfMetadataTrait;
use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class EmergencyLevelWithTimeLimitZero implements Rule
{
    use SelfMetadataTrait;
    use MetadataTrait;

    /**
     * @var null
     */
    private $emergencyLevelId;

    /**
     * Create a new rule instance.
     *
     * @param null $emergencyLevelId
     */
    public function __construct($emergencyLevelId = null)
    {
        //
        $this->emergencyLevelId = $emergencyLevelId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value == 0) {

            $emergencyLevelWithTimeLimitZero = $this->getEmergencyLevelWithTimeLimitZero();

            // Check if emergencyLevel with time_limit = 0 exist
            // Check if we are performing update operation &&
            // Check if $emergencyLevelWithTimeLimitZero founded is the same that the emergencyLevel we want to update
            if (!is_null($emergencyLevelWithTimeLimitZero) && !(!is_null($this->emergencyLevelId) && $emergencyLevelWithTimeLimitZero['id'] == $this->emergencyLevelId)) {
                return false;
            }

        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'An emergency level with time limit 0 already exist';
    }
}
