<?php

namespace App\Traits;

use App\Models\Breakdown;
use Cmdtaz\Metadata\Rules\MetadataExists;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

trait ResolutionTrait
{

    public function getRules(Request $request, Breakdown $breakdown)
    {
        return [
            'operation_type' => [Rule::requiredIf($request->resolved), new MetadataExists('operation-types'), 'uuid'],
            'diagnostic' => [Rule::requiredIf($request->resolved), 'string'],
            'resolution' => [Rule::requiredIf($request->resolved), 'string'],
            'observations' => [Rule::requiredIf($request->resolved), 'string'],
            'result' => [Rule::requiredIf($request->resolved), new MetadataExists('results'), 'uuid'],
            'next_step' => [Rule::requiredIf($request->resolved), new MetadataExists('next-steps'), 'uuid'],
            'other_cost' => ['nullable', 'integer', 'numeric', 'min:0'],
            'parts_cost' => ['nullable', 'integer', 'numeric', 'min:0'],
            'parts' => ['nullable', 'array'],
            'parts.*.number' => ['required', 'integer', 'numeric', 'min:1'],
            'parts.*.name' => ['required', 'string'],
            'parts.*.unit_price' => ['required', 'numeric', 'min:1'],
            'duration' => [Rule::requiredIf($request->resolved), 'integer', 'numeric', 'min:1'],
            'performers' => [Rule::requiredIf($request->resolved && $breakdown->service == 'internal'), 'array'],
            'performers.*' => ['uuid'],
            'bill_number' => [Rule::requiredIf($request->resolved && $breakdown->service == 'external'), 'string'],
            'resolved' => ['required', 'boolean'],
        ];
    }

}
