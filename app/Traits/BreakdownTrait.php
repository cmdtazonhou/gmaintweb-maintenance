<?php

namespace App\Traits;

use App\Models\Breakdown;

trait BreakdownTrait
{

    /**
     * @param \App\Models\Breakdown $breakdown
     * @return mixed
     */
    public function getBreakdown(Breakdown $breakdown)
    {
        $breakdown->load('materialCategory', 'material', 'serviceProvider');

        $metadataRelationshipArray = [];

        $metadataFields = [
            'emergency_level',
            'operation_type',
            'result',
            'next_step',
            'operation_title'
        ];

        foreach ($metadataFields as $field) {
            if ($breakdown->{$field}) {
                $metadataRelationshipArray[] = $field;
            }
        }

        $breakdown = $this->metadataRelationship($metadataRelationshipArray, $breakdown);

        $data['breakdown'] = $breakdown;

        $data['availableContract'] = optional($this->getEquipmentAvailableContract($breakdown->equipment_id))->load('serviceProvider', 'contractType');

        $data['breakdownsCount'] = Breakdown::nonPreventiveMaintenance()
            ->where('equipment_id', $breakdown->equipment_id)
            ->get()
            ->count();

        return $data;
    }

    /**
     * @param Breakdown $breakdown
     * @return mixed|string
     */
    public function getActivityType(Breakdown $breakdown)
    {
        if ($breakdown->maintenance_type == 'preventive' || $breakdown->maintenance_type == 'user-preventive') {
            return $breakdown->maintenance_type;
        }

        return is_null($breakdown->material) ? 'corrective' : 'others';
    }

    public function getMaintenanceTypes()
    {
        return [
            'preventive' => __("Maintenance Preventive"),
            'user-preventive' => __("Maintenance Utilisateur"),
            'corrective' => __("Maintenance Corrective"),
            'others' => __("Travaux Annexes")
        ];
    }

}
