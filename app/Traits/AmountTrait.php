<?php

namespace App\Traits;

use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Support\Arr;

trait AmountTrait
{

    public function amount($priceHT)
    {
        $amountMetadata = $this->getMetadataData(Metadata::all(), 'amount');

        $discount = intval($this->getAmountParam($amountMetadata, 'discount')['value']);
        $tva_percent = intval($this->getAmountParam($amountMetadata, 'tva_percent')['value']);

        return (intval($priceHT) - $discount) + (intval($priceHT) - $discount)*($tva_percent/100);
    }

    /**
     * @param $amountMetadata
     * @param $name
     * @return mixed
     */
    public function getAmountParam($amountMetadata, $name)
    {
        return Arr::first(
            $amountMetadata,
            function ($value, $key) use ($name) {
                return $value['name'] == $name;
            },
            null
        );
    }

}
