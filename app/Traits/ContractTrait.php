<?php

namespace App\Traits;

use App\Models\Contract;
use Carbon\Carbon;
use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Http\Request;

trait ContractTrait
{

    /**
     * @param $equipmentId
     * @return mixed
     */
    public function getEquipmentAvailableContract($equipmentId)
    {
        return Contract::where('equipment_id', $equipmentId)
            ->notArchived()
            ->get()
            ->first(function ($contract, $key) {
                return Carbon::now()->between($contract->started_at, $contract->started_at->addMonths(intval($contract->delay)));
            });
    }

    /**
     * @param Contract $contract
     * @return bool
     */
    public function expireSoon(Contract $contract)
    {
        $contractTimeRemainingAlert = $this->getMetadataData(Metadata::all(), 'contract-time-remaining')->all()[0]['value'];

        return Carbon::now()->addMonths(intval($contractTimeRemainingAlert)) >= $this->contractExpiryDate($contract);

    }

    /**
     * @param Contract $contract
     * @param $service_provider_id
     * @return bool
     */
    public function executedBy(Contract $contract, $service_provider_id)
    {
        return $contract->service_provider_id == $service_provider_id;
    }

    /**
     * @param Contract $contract
     * @return bool
     */
    public function contractInProgress(Contract $contract)
    {
        return $this->getStatus($contract) === 'in-progress';
    }

    /**
     * @param Contract $contract
     * @return bool
     */
    public function contractExpired(Contract $contract)
    {
        return $this->getStatus($contract) === 'expired';
    }

    /**
     * @param Contract $contract
     * @return bool
     */
    public function contractArchived(Contract $contract)
    {
        return $this->getStatus($contract) === 'archived';
    }

    /**
     * @param $period
     * @param Contract $contract
     * @return bool
     */
    public function contractSignedInPeriod($period, Contract $contract)
    {
        $result = true;

        if (array_key_exists('start', $period)) {
            $result = $result && Carbon::parse($period['start'])->lte($contract->started_at);
        }

        if (array_key_exists('end', $period)) {
            $result = $result && Carbon::parse($period['end'])->gte($contract->started_at);
        }

        return $result;
    }

    /**
     * @param $period
     * @param Contract $contract
     * @return bool
     */
    public function contractExpiredInPeriod($period, Contract $contract)
    {
        $result = true;

        if (array_key_exists('start', $period)) {
            $result = $result && Carbon::parse($period['start'])->lte($this->contractExpiryDate($contract));
        }

        if (array_key_exists('end', $period)) {
            $result = $result && Carbon::parse($period['end'])->gte($this->contractExpiryDate($contract));
        }

        return $result;
    }

    /**
     * @param $period
     * @param Contract $contract
     * @return bool
     */
    public function contractSignedAndExpiredInPeriod($period, Contract $contract)
    {
        return $this->contractSignedInPeriod($period, $contract) && $this->contractExpiredInPeriod($period, $contract);
    }

    /**
     * Determine the status of the contract.
     *
     * @param Contract $contract
     * @return string
     */
    public function getStatus(Contract $contract)
    {
        try {
            if ($contract->archived_at) {
                return 'archived';
            }

            if (Carbon::now()->gt($this->contractExpiryDate($contract))) {
                return 'expired';
            }
        } catch (\Exception $exception) {
        }

        return 'in-progress';
    }

    /**
     * @param Contract $contract
     * @return mixed
     */
    public function contractExpiryDate(Contract $contract)
    {
        return $contract->started_at->addMonths(intval($contract->delay));
    }

    /**
     * @param Request $request
     * @param Contract $contract
     * @return bool
     */
    public function filterContract(Request $request, Contract $contract)
    {

        $result = true;

        if ($request->filled('start')) {

            $period = $request->only(['start', 'end']);

            if ($request->event_in_period == 'signed') {
                $result = $result && $this->contractSignedInPeriod($period, $contract);
            } elseif ($request->event_in_period == 'expired') {
                $result = $result && $this->contractExpiredInPeriod($period, $contract);
            } else {
                $result = $result && $this->contractSignedAndExpiredInPeriod($period, $contract);
            }

        }

        if ($request->filled('status')) {
            $result = $result && ($request->status == $this->getStatus($contract));
        }

        if ($request->filled('service_provider_id')) {
            $result = $result && $this->executedBy($contract, $request->service_provider_id);
        }

        return $result;
    }

}
