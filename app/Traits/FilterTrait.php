<?php

namespace App\Traits;

use App\Models\Breakdown;
use Carbon\Carbon;
use Illuminate\Http\Request;

trait FilterTrait
{

    /**
     * @return array
     */
    public function getYears()
    {
        $years = [];
        for ($year = 1990; $year <= Carbon::now()->year; $year++) {
            $years[] = $year;
        }
        return $years;
    }

    /**
     * @return array
     */
    public function getMonths()
    {
        $months = [];

        $monthsName = [
            __("Janvier"),
            __("Février"),
            __("Mars"),
            __("Avril"),
            __("Mai"),
            __("Juin"),
            __("Juillet"),
            __("Août"),
            __("Septembre"),
            __("Octobre"),
            __("Novembre"),
            __("Décembre")
        ];

        for ($month = 1; $month <= 12; $month++) {
            $months[$monthsName[$month - 1]] = $month;
        }

        return $months;
    }
}
