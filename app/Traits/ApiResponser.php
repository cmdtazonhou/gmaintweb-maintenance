<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

trait ApiResponser
{
    protected function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    protected function errorResponse($message, $code)
    {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function response($response)
    {
        if ($response->failed())
            return response()->json($response->json(), $response->json()["code"]);

        return response()->json($response->json(), Response::HTTP_OK);
    }

    protected function arrayResponse($response)
    {
        if ($response->failed()) {
            return response()->json($response->json(), $response->json()["code"]);
        }

        return $response->json();
    }

    protected function responseMultipartRequest($response)
    {
        return response()->json(json_decode($response->getBody()), $response->getStatusCode());
    }

}
