<?php

namespace App\Traits;

use Cmdtaz\Metadata\Models\Metadata;
use Illuminate\Support\Arr;

trait SelfMetadataTrait
{

    public function getEmergencyLevelWithTimeLimitZero()
    {
        $emergencyLevels = $this->getMetadataData(Metadata::all(), 'emergency-levels');

        return Arr::first(
            $emergencyLevels,
            function ($emergencyLevel, $key) {
                return $emergencyLevel['time_limit'] == 0;
            },
            null
        );
    }

}
