<?php


namespace App\Traits;


trait phoneNumbers
{
    public function phoneNumbersStartedWith()
    {
        return ['99', '98', '96', '95', '94', '93', '91', '90', '69', '68', '67', '66', '65', '64', '63', '62', '61', '60', '51'];
    }
}
