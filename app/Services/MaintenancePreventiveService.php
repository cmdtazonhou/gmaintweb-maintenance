<?php


namespace App\Services;


use App\Models\Breakdown;
use Carbon\Carbon;
use Illuminate\Support\Arr;


class MaintenancePreventiveService
{

    public function scheduleMaintenancePreventive(array $data)
    {
        $breakdown = Breakdown::create([
            'equipment_id' => $data['equipment_id'],
            'operation_type' => ["metadataName" => "operation-types", "dataId" => $data['operation_type']],
            'operation_title' => ["metadataName" => "operation-titles", "dataId" => $data['operation_title']],
            'room_id' => $data['room_id'],
            'created_by' => $data['created_by'],
            'entity_id' => $data['entity_id'],
            'status' => Breakdown::SCHEDULED,
            'scheduled_at' => Carbon::now(),
            'scheduled_intervention_date' => Carbon::createFromFormat('d-m-Y H:i', $data['scheduled_intervention_date'])->format('Y-m-d H:i'),
            'maintenance_type' => 'preventive',
            'duration' => $data['duration'],
            'periodicity_id' => $data['periodicity_id']
        ]);

        return $breakdown;
    }

    /**
     * @param array $data
     * @param Breakdown $breakdown
     * @return Breakdown
     */
    public function updateMaintenancePreventive(array $data, Breakdown $breakdown)
    {

        $fillable = ['other_cost'];

        if ($data['is_done']) {

            $now = Carbon::now()->format('Y-m-d H:i');

            $data['effective_intervention_date'] = Carbon::createFromFormat('d-m-Y H:i', $data['effective_intervention_date'])->format('Y-m-d H:i');
//            $data['resolved_at'] = $now;
            $data['archived_at'] = $now;
            $data['archived_by'] = $data['resolved_by'];
            $data['status'] = Breakdown::ARCHIVED;

            array_push($fillable,
                'actions_performed',
                'service',
//                'resolved_by',
                'effective_intervention_date',
//                'resolved_at',
                'status',
                'archived_at',
                'archived_by'
            );

            if ($data['service'] == 'internal') {

                array_push($fillable,
                    'performers'
                );

            } else {

                array_push($fillable,
                    'service_provider_id',
                    'service_provider_performer'
                );

            }

            $breakdown->update(Arr::only($data, $fillable));

            $upcomingBreakdownData = $breakdown->toArray();
            $upcomingBreakdownData['scheduled_intervention_date'] = $data['upcoming_schedule_intervention_date'];

            return $this->scheduleMaintenancePreventive($upcomingBreakdownData);

        }

        $breakdown->update(Arr::only($data, $fillable));

        return $breakdown;
    }

    public function cancelMaintenancePreventive($equipmentId, array $data)
    {
        $now = Carbon::now();

        $maintenancePreventives = Breakdown::preventiveMaintenance()
            ->notArchived()
            ->where('equipment_id', $equipmentId)
            ->update([
                'status' => Breakdown::ARCHIVED,
                'canceled_at' => $now,
                'canceled_reason' => __("Entretien préventif non nécessaire"),
                'effective_intervention_date' => $now,
                'archived_at' => $now,
                'archived_by' => $data['archived_by']
            ]);

        return $maintenancePreventives;
    }

}
