<?php


namespace App\Services;


use App\Models\Breakdown;
use App\Traits\ContractTrait;
use App\Traits\SelfMetadataTrait;
use Carbon\Carbon;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Validation\Rule;

class CorrectiveMaintenanceService
{
    use SelfMetadataTrait;
    use ContractTrait;
    use MetadataTrait;

    public function getScheduleMaintenanceCorrectiveRules(array $data)
    {
        $emergencyLevelWithTimeLimitZero = $this->getEmergencyLevelWithTimeLimitZero();

        $emergencyLevelId = array_key_exists('emergency_level', $data) ? $data['emergency_level'] : null;
        $service = array_key_exists('service', $data) ? $data['service'] : null;

        $isEmergencyLevelValid = $emergencyLevelId && $emergencyLevelId != optional($emergencyLevelWithTimeLimitZero)['id'];

        return [
            'scheduled_by' => [
                Rule::requiredIf($isEmergencyLevelValid),
                'uuid'
            ],
            'service' => [
                Rule::requiredIf($isEmergencyLevelValid),
                Rule::in(['external', 'internal'])
            ],
            'service_provider_id' => [
                Rule::requiredIf(
                    $isEmergencyLevelValid
                    && $service == 'external'
                ),
                'exists:service_providers,id'
            ],
        ];
    }

    public function storeMaintenanceCorrective(array $data)
    {
        $breakdownData = array_merge(
            [
                'material_category_id' => $data['material_category_id'],
                'equipment_id' => $data['equipment_id'],
                'description' => $data['description'],
                'room_id' => $data['room_id'],
                'created_by' => $data['created_by'],
                'entity_id' => $data['entity_id'],
                'maintenance_type' => 'non-preventive'
            ],
            $this->calculateDataForMaintenanceCorrectiveStoring($data)
        );

        return Breakdown::create($breakdownData);
    }

    public function calculateDataForMaintenanceCorrectiveStoring(array $data)
    {
        $emergencyLevelWithTimeLimitZero = $this->getEmergencyLevelWithTimeLimitZero();

        $emergencyLevel = array_key_exists('emergency_level', $data) ? $data['emergency_level'] : null;

        $status = !$emergencyLevel
            ? Breakdown::UNSCHEDULED
            : (($emergencyLevel == optional($emergencyLevelWithTimeLimitZero)['id']) ? Breakdown::RESCHEDULED : null);

        $palliative_actions = array_key_exists('palliative_actions', $data) ? $data['palliative_actions'] : null;

        return compact('status', 'palliative_actions');
    }

    public function calculateDataForMaintenanceCorrectiveScheduling(array $data)
    {
        $availableContract = array_key_exists('equipment_id', $data) ? $this->getEquipmentAvailableContract($data['equipment_id']) : null;

        if ($availableContract) {
            $service = 'external';
            $service_provider_id = $availableContract->service_provider_id;
            return compact('service', 'service_provider_id');
        }

        $return = [];
        if (array_key_exists('service', $data)) {

            $return['service'] = $data['service'];

            if (array_key_exists('service_provider_id', $data) && $data['service'] == 'external') {
                $return['service_provider_id'] = $data['service_provider_id'];
            }

        }

        return $return;
    }

    public function scheduleMaintenanceCorrective(array $data, Breakdown $breakdown)
    {
        $emergencyLevel = $this->findData('emergency-levels', $data['emergency_level']);

        $currentDate = Carbon::now();

        $scheduledData = [
            'emergency_level' => ["metadataName" => "emergency-levels", "dataId" => $data['emergency_level']],
            'status' => Breakdown::SCHEDULED,
            'service' => $data['service'],
            'scheduled_by' => $data['scheduled_by'],
            'scheduled_at' => $currentDate,
            'scheduled_intervention_date' => $currentDate->addDays(intval($emergencyLevel['time_limit']))
        ];

        if (array_key_exists('service_provider_id', $data) && $scheduledData['service'] == 'external') {
            $scheduledData['service_provider_id'] = $data['service_provider_id'];
        }

        $breakdown->update($scheduledData);

        return $breakdown->refresh();
    }

    public function getUnscheduledMaintenanceCorrectives($entityId)
    {
        return Breakdown::with('materialCategory', 'material')
            ->nonPreventiveMaintenance()
            ->localised($entityId)
            ->whereIn('status', [Breakdown::UNSCHEDULED, Breakdown::RESCHEDULED])
            ->get();
    }

    public function getScheduledMaintenanceCorrectives($entityId)
    {
        return Breakdown::with('materialCategory', 'material')
            ->nonPreventiveMaintenance()
            ->localised($entityId)
            ->whereIn('status', [Breakdown::SCHEDULED])
            ->get();
    }

    public function getResolvedMaintenanceCorrectives($entityId)
    {
        return Breakdown::with('materialCategory', 'material')
            ->nonPreventiveMaintenance()
            ->localised($entityId)
            ->whereIn('status', [Breakdown::DONE])
            ->get();
    }
}
