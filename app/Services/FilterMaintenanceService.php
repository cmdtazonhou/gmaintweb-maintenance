<?php


namespace App\Services;


use App\Models\Breakdown;
use App\Traits\BreakdownTrait;
use App\Traits\ContractTrait;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\Request;


class FilterMaintenanceService
{

    use MetadataTrait;
    use BreakdownTrait;
    use ContractTrait;


    /**
     * @param Request $request
     * @return array
     */
    public function filterMaintenance(Request $request)
    {
        $breakdowns = Breakdown::where('status', Breakdown::ARCHIVED)
            ->get()
            ->filter(function ($breakdown, $key) use ($request) {

                $activityType = $this->getActivityType($breakdown);

                $breakdown->activity_type = $activityType;

                $filter = in_array($activityType, ['preventive', 'corrective', 'others']);

                if ($request->filled('entity_id')) {
                    $filter = $filter && $breakdown->entity_id == $request->entity_id;
                }

                if ($request->filled('room_ids')) {
                    $filter = $filter && in_array($breakdown->room_id, $request->room_ids);
                }

                if ($request->filled('year')) {
                    $filter = $filter && $breakdown->resolved_at->year == $request->year;
                }

                if ($request->filled('month')) {
                    $filter = $filter && $breakdown->resolved_at->month == $request->month;
                }

                if ($request->filled('maintenance_type')) {
                    $filter = $filter && $activityType == $request->maintenance_type;
                }

                $breakdown = $this->getBreakdown($breakdown);

                return $filter;
            });

        return $breakdowns;
    }

}
