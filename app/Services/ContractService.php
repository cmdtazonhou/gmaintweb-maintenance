<?php


namespace App\Services;


use App\Models\Contract;

class ContractService
{
    public function storeContract(array $array)
    {
        return Contract::create([
            'equipment_id' => $array['equipment_id'],
            'contract_type_id' => $array['contract_type_id'],
            'service_provider_id' => $array['service_provider_id'],
            'started_at' => $array['started_at'],
            'delay' => $array['delay'],
            'entity_id' => $array['entity_id']
        ]);
    }
}
