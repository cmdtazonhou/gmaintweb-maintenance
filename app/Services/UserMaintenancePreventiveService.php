<?php


namespace App\Services;


use App\Models\Breakdown;
use Carbon\Carbon;
use Illuminate\Http\Request;


class UserMaintenancePreventiveService
{

    public function storeUserMaintenancePreventive(Request $request)
    {
        $data = $request->only([
            'entity_id',
            'equipment_id',
            'room_id',
            'created_by',
            'actions_performed',
            'observations'
        ]);

        $data['effective_intervention_date'] = Carbon::now();
        $data['maintenance_type'] = 'user-preventive';
        $data['status'] = Breakdown::DONE;

        $breakdown = Breakdown::create($data);

        return $breakdown;
    }

}
