<?php


namespace App\Services;


use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;

class ResultService
{
    use MetadataTrait;

    /**
     * @param array $data
     * @throws \Exception
     */
    public function update(array $data)
    {
        $resultsData = $this->getMetadataData(Metadata::all(), 'results')->all();

        foreach ($resultsData as $key => $resultData){
            if ($resultData['value'] == 'conform'){
                $resultsData[$key]['name'] = $data['conform_name'];
            }

            if ($resultData['value'] == 'improper'){
                $resultsData[$key]['name'] = $data['improper_name'];
            }
        }


        $this->getModel()->update(['data' => $resultsData]);
    }

    public function find($id)
    {
        return $this->findData('results', $id);
    }

    public function getModel()
    {
        return Metadata::where('name', 'results')->firstOrFail();
    }


}
