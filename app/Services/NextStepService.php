<?php


namespace App\Services;


use Cmdtaz\Metadata\Models\Metadata;
use Cmdtaz\Metadata\Traits\MetadataTrait;
use Illuminate\Http\JsonResponse;

class NextStepService
{
    use MetadataTrait;

    public function getReformedNextStep()
    {
        $metadataCollection = Metadata::all();

        $nextSteps = $this->getMetadataData($metadataCollection, "next-steps")->all();

        foreach ($nextSteps as $nextStep) {
            if ($nextStep['code'] == 'reformed') {
                return $nextStep;
            }
        }

        return null;
    }

    public function create(array $data)
    {
        $this->storeMetadataData($data, $this->getModel());
    }

    /**
     * @param array $data
     * @param $id
     * @throws \Exception
     */
    public function update(array $data, $id)
    {
        $metadata = $this->updateMetadataData($data, $this->getModel(), $id);

        if (!$metadata){
            throw new \Exception('Can not found any next-step with the given id', JsonResponse::HTTP_NOT_FOUND);
        }

    }

    public function getModel()
    {
        return Metadata::where('name', 'next-steps')->firstOrFail();
    }

    public function find($id)
    {
        return $this->findData('next-steps', $id);
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function delete($id)
    {
        $data = $this->find($id);

        if ($data) {
            $this->destroyMetadataData($this->getModel(), $id);
            return $data;
        }

        throw new \Exception('Can not found any next-step with the given id', JsonResponse::HTTP_NOT_FOUND);

    }

}
